(*****************************************************************************)
(*                                                                           *)
(* MIT License                                                               *)
(*                                                                           *)
(* Copyright (c) 2021 Functori - contact@functori.com                        *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining     *)
(* a copy of this software and associated documentation files (the           *)
(* "Software"), to deal in the Software without restriction, including       *)
(* without limitation the rights to use, copy, modify, merge, publish,       *)
(* distribute, sublicense, and/or sell copies of the Software, and to        *)
(* permit persons to whom the Software is furnished to do so, subject to     *)
(* the following conditions:                                                 *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be            *)
(* included in all copies or substantial portions of the Software.           *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,           *)
(* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF        *)
(* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND                     *)
(* NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE    *)
(* LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION    *)
(* OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION     *)
(* WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.           *)
(*                                                                           *)
(*****************************************************************************)

open Rp
open Proto

type config = {
  mutable silent: bool;
  mutable node: EzAPI.base_url;
  mutable constants: constants option;
  mutable chain_id: A.chain_id option;
}

type forge = < branch : A.block_hash; contents : script_expr manager_operation list >
[@@deriving encoding]

type preapply = <
  branch: A.block_hash option;
  contents: script_expr manager_operation list;
  protocol: A.protocol_hash option;
  signature: A.signature option;
> list [@@deriving encoding {option="opt"}]

type run_input = <
  operation: <
    branch: A.block_hash;
    contents: script_expr manager_operation list;
    signature: A.signature
  >;
  chain_id: A.chain_id
> [@@deriving encoding]

type run_output = <
  contents: micheline manager_operation list;
  signature: A.signature option
> [@@deriving encoding]

type run_view_input = <
  contract: A.contract;
  entrypoint: entrypoint;
  input: micheline;
  chain_id: A.chain_id;
  unparsing_mode: [`Readable | `Optimized | `Optimized_legacy ]; [@enum]
  source: A.contract option;
  payer: A.contract option;
  now: A.timestamp option;
  level: int32 option;
> [@@deriving encoding]

type run_view_output = (micheline [@wrap "data"]) [@@deriving encoding]

type run_code_input = <
  script: micheline;
  storage: micheline;
  input: micheline;
  amount: A.uint64;
  balance: A.uint64;
  chain_id: A.chain_id;
  entrypoint: entrypoint option;
  source: A.contract option;
  payer: A.contract option;
  now: A.timestamp option;
  level: int32 option;
> [@@deriving encoding]

type run_code_output = <
  storage: micheline;
  operations: micheline manager_operation list;
  lazy_storage_diff: storage_diff_item list option;
> [@@deriving encoding]

type entrypoints = (string * micheline) list [@assoc] [@obj1 "entrypoints"]
[@@deriving encoding {ignore}]

type header_block = <
  hash : A.block_hash;
  protocol : A.protocol_hash;
  chain_id : A.chain_id;
> [@@deriving encoding {ignore}]

type forge_method = [ `remote | `local | `both ]

let config = {
  silent = false;
  node = EzAPI.BASE "https://tz.functori.com";
  constants = None;
  chain_id = None;
}

let set_silent b = config.silent <- b
let set_node s = config.node <- EzAPI.BASE s

module Services = struct

  open EzAPI

  let register = false
  let arg_hash = Arg.string "hash"
  let arg_id = Arg.make ~name:"id" ~destruct:(fun s -> Ok (Z.of_string s)) ~construct:Z.to_string ()
  let blocks_path = Path.(root // "chains" // "main" // "blocks")
  let head_path = Path.(blocks_path // "head")
  let ctxt_path = Path.(head_path // "context")

  let errors = [
    Err.make ~code:500 ~name:"node_error" ~encoding:(Json_encoding.list node_error_enc.json)
      ~select:(function `node_error e -> Some e | _ -> None)
      ~deselect:(fun e -> `node_error e);
  ]

  type nonrec 'a service0 = ('a, Rp.error, no_security) service0
  type nonrec ('a, 'b) service1 = ('a, 'b, Rp.error, no_security) service1
  type nonrec ('a, 'b, 'c) service2 = ('a, 'b, 'c, Rp.error, no_security) service2
  type nonrec ('a, 'b) post_service0 = ('a, 'b, Rp.error, no_security) post_service0
  type nonrec ('a, 'b, 'c) post_service1 = ('a, 'b, 'c, Rp.error, no_security) post_service1

  let account_info : (A.block_hash, A.contract, account) service2 =
    service ~register ~output:account_enc.Encoding.json ~errors
      Path.(blocks_path /: arg_hash // "context" // "contracts" /: arg_hash )

  let header : (A.block_hash, header_block) service1 =
    service ~register ~output:header_block_enc.Encoding.json ~errors
      Path.(blocks_path /: arg_hash // "header")

  let head_hash : A.block_hash service0 =
    service ~register ~output:Json_encoding.string ~errors
      Path.(blocks_path // "head" // "hash")

  let constants : (A.block_hash, constants) service1 =
    service ~register ~output:constants_enc.Encoding.json ~errors
      Path.(blocks_path /: arg_hash // "context" // "constants")

  let manager_key : (A.block_hash, A.contract, A.pk option) service2 =
    service ~register ~output:Json_encoding.(option Json_encoding.string) ~errors
      Path.(blocks_path /: arg_hash // "context" // "contracts" /: arg_hash // "manager_key")

  let entrypoints : (string, entrypoints) service1 =
    service ~register ~output:entrypoints_enc.Encoding.json ~errors
      Path.(ctxt_path // "contracts" /: arg_hash // "entrypoints")

  let entrypoint_type : (A.contract, string, micheline) service2 =
    service ~register ~output:micheline_enc.Encoding.json ~errors
      Path.(ctxt_path // "contracts" /: arg_hash // "entrypoints" /: Arg.string "name")

  let storage : (A.block_hash, A.contract, micheline) service2 =
    service ~register ~output:micheline_enc.Encoding.json ~errors
      Path.(blocks_path /: arg_hash // "context" // "contracts" /: arg_hash // "storage")

  let big_map : string -> (A.zarith, A.contract, micheline) service2 = fun block ->
    service ~register ~output:micheline_enc.Encoding.json ~errors
      Path.(blocks_path  // block // "context" // "big_maps" /: arg_id /: arg_hash)

  let preapply : (preapply, preapply) post_service0 =
    post_service ~register ~input:preapply_enc.Encoding.json ~output:preapply_enc.Encoding.json ~errors
      Path.(head_path // "helpers" // "preapply" // "operations")

  let run_operation : (run_input, run_output) post_service0 =
    post_service ~register ~input:run_input_enc.Encoding.json ~output:run_output_enc.Encoding.json ~errors
      Path.(head_path // "helpers" // "scripts" // "run_operation")

  let run_view : (run_view_input, micheline) post_service0 =
    post_service ~register ~input:run_view_input_enc.Encoding.json ~output:run_view_output_enc.Encoding.json ~errors
      Path.(head_path // "helpers" // "scripts" // "run_view")

  let run_code : (run_code_input, run_code_output) post_service0 =
    post_service ~register ~input:run_code_input_enc.Encoding.json ~output:run_code_output_enc.Encoding.json ~errors
      Path.(head_path // "helpers" // "scripts" // "run_code")

  let inject : (hex, A.operation_hash) post_service0 =
    post_service ~register ~input:Encoding.hex_json ~output:Json_encoding.string ~errors
      Path.(root // "injection" // "operation")

  let forge : (A.contract, forge, hex) post_service1 =
    post_service ~register ~input:forge_enc.Encoding.json ~output:Encoding.hex_json ~errors
      Path.(blocks_path /: arg_hash // "helpers" // "forge" // "operations")
end

let wrap_http code content = Error (`http_error (code, content))

let wrap_raw = function
  | Error (500, Some s) ->
    begin match EzEncoding.destruct_res (Json_encoding.list node_error_enc.json) s with
      | Error _ -> wrap_http 400 (Some s)
      | Ok l -> Error (`node_error l)
    end
  | Error (code, content) -> wrap_http code content
  | Ok x -> Ok x

let wrap = function
  | Error EzReq_lwt_S.(UnknownError {code; msg}) -> wrap_http code msg
  | Error EzReq_lwt_S.(KnownError {error; _}) -> Error error
  | Ok x -> Ok x

let rmap f l =
  let rec aux acc = function
    | [] -> Ok (List.rev acc)
    | h :: q -> match f h with
      | Error e -> Error e
      | Ok h -> aux (h :: acc) q in
  aux [] l

let rec unmacro = function
  | Mprim { prim = `macro s; args; annots } -> Forge.Macros.expand ~args ~annots s
  | Mprim { prim; args; annots } ->
    begin match rmap unmacro args with
      | Error e -> Error e
      | Ok args -> Ok (Mprim { prim; args; annots })
    end
  | Mseq l ->
    begin match rmap unmacro l with
      | Error e -> Error e
      | Ok l -> Ok (Mseq l)
    end
  | m -> Ok m

let unmacro_ops ops =
  rmap (fun o ->
      match o.man_info.kind with
      | Origination ({ script = { code = Micheline m; _ }; _ } as ori) ->
        begin match unmacro m with
          | Error e -> Error e
          | Ok m ->
            let script = { ori.script with code = Micheline m } in
            let kind = Origination { ori with script } in
            let man_info = {  o.man_info with kind } in
            Ok { o with man_info }
        end
      | _ -> Ok o) ops

let get ?msg ?(base=config.node) url =
  let EzAPI.BASE base = base in
  let msg = match msg with | Some msg when not config.silent -> Some msg | _ -> None in
  let|> r = EzReq_lwt.get ?msg (EzAPI.URL (base ^ url)) in wrap_raw r
let get0 ?msg ?(base=config.node) s =
  let msg = match msg with | Some msg when not config.silent -> Some msg | _ -> None in
  let|> r = EzReq_lwt.get0 ?msg base s in wrap r
let get1 ?msg ?(base=config.node) s arg =
  let msg = match msg with | Some msg when not config.silent -> Some msg | _ -> None in
  let|> r = EzReq_lwt.get1 ?msg base s arg in wrap r
let get2 ?msg ?(base=config.node) s a1 a2 =
  let msg = match msg with | Some msg when not config.silent -> Some msg | _ -> None in
  let|> r = EzReq_lwt.get2 ?msg base s a1 a2 in wrap r

let get_account_info ?base ?(block="head") hash = get2 ~msg:"account" ?base Services.account_info block hash
let get_header ?base ?(block="head") () = get1 ~msg:"header" ?base Services.header block
let get_head_hash ?base () = get0 ~msg:"head_hash" ?base Services.head_hash
let get_raw_rpc ?base rpc = get ?base rpc
let get_enc_rpc ?base enc rpc =
  let>? s = get ?base rpc in
  Lwt.return (EzEncoding.destruct_res enc s)
let get_constants ?base ?(block="head") () = get1 ~msg:"constants" ?base Services.constants block
let get_manager_key ?base ?(block="head") hash = get2 ~msg:"manager-key" ?base Services.manager_key block hash
let get_entrypoints ?base hash = get1 ~msg:"entrypoints" ?base Services.entrypoints hash
let get_entrypoint_type ?base hash ep = get2 ~msg:"entrypoint_type" ?base Services.entrypoint_type hash ep
let get_storage ?base ?(block="head") hash = get2 ~msg:"storage" ?base Services.storage block hash
let get_bigmap_value_raw ?base ?(block="head") id hash =
  let|> r = get2 ~msg:"bigmap" ?base (Services.big_map block) id hash in
  match r with
  | Error (`http_error (404, _)) -> Ok None
  | Error e -> Error e
  | Ok x -> Ok (Some x)
let get_bigmap_value ?base ?block ~typ id key =
  match Forge.pack typ key with
  | Error e -> Lwt.return_error e
  | Ok b ->
    let h = Crypto.(Base58.encode ~prefix:Prefix.script_expr_hash @@ blake2b_32 [ b ]) in
    get_bigmap_value_raw ?base ?block id h

let post ?content ?content_type ?msg ?(base=config.node) url =
  let EzAPI.BASE base = base in
  let msg = match msg with | Some msg when not config.silent -> Some msg | _ -> None in
  let|> r = EzReq_lwt.post ?content ?content_type ?msg (EzAPI.TYPES.URL (base ^ url)) in
  wrap_raw r

let post0 ?msg ~input ?(base=config.node) s =
  let msg = match msg with | Some msg when not config.silent -> Some msg | _ -> None in
  let|> r = EzReq_lwt.post0 ?msg ~input base s in wrap r
let post1 ?msg ~input ?(base=config.node) s arg =
  let msg = match msg with | Some msg when not config.silent -> Some msg | _ -> None in
  let|> r = EzReq_lwt.post1 ?msg ~input base s arg in wrap r

let set_constants ?base ?block () =
  let|>? constants = get_constants ?base ?block () in
  config.constants <- Some constants

let set_chain_id ?base ?block () =
  let|>? header = get_header ?base ?block () in
  config.chain_id <- Some header#chain_id

let preapply ?base ~branch ~protocol ~signature ops =
  let input = [
    object method branch = Some branch method contents = ops
      method protocol = Some protocol method signature = Some signature
    end ] in
  post0 ~msg:"preapply" ~input ?base Services.preapply

let dummy_sign =
  "edsigtXomBKi5CTRf5cjATJWSyaRvhfYNHqSUGrn4SdbYRcGwQrUGjzEfQDTuqHhuA8b2d8NarZjz8TRf65WkpQmo423BtomS8Q"

let run_operation ?base ~branch ~chain_id ops =
  let operation = object method branch = branch method contents = ops method signature = dummy_sign end in
  let input = object method operation = operation method chain_id = chain_id end in
  let|>? r = post0 ~msg:"run-operation" ~input ?base Services.run_operation in
  r#contents

let silent_inject ?base (bytes : Crypto.Raw.t) : (string, _) result Lwt.t =
  let open Crypto in
  let expected_hash = blake2b_32 [ bytes ] in
  let>? h = post0 ?base ~input:(hex_of_raw bytes) ~msg:"inject" Services.inject in
  let hash = Base58.decode ~prefix:Prefix.operation_hash h in
  if not (hash = expected_hash) then
    Lwt.return_error `unexpected_operation_hash
  else Lwt.return_ok h

let check_status op =
  match op.man_metadata with
  | None -> Error `no_metadata
  | Some meta ->
    if meta.man_operation_result.op_status = `applied then Ok meta
    else Error (`node_error meta.man_operation_result.op_errors)

let sign ?(watermark=Crypto.Watermark.generic) ~edsk bytes =
  Lwt.return @@
  Result.bind (Crypto.Curve.from_b58 edsk) @@ function
  | `ed25519 ->
    Result.bind (Crypto.Sk.b58dec edsk) @@ fun sk ->
    Result.bind (Crypto.Ed25519.sign_bytes ~watermark ~sk bytes) @@ fun b ->
    Ok (b :> Crypto.Raw.t)
  | _ -> Error `unknown_curve

let inject ?base ~sign ~bytes ~branch ~protocol ops =
  let>? signature_bytes = sign bytes in
  let signature = Crypto.(Base58.encode ~prefix:Prefix.ed25519_signature signature_bytes) in
  let>? l = preapply ?base ~branch ~protocol ~signature ops in
  let rec aux f = function [] -> Ok () | h :: t -> match f h with
    | Ok _ -> aux f t | Error e -> Error e in
  let r = aux (fun r -> aux check_status r#contents) l in
  match r with
  | Error e -> Lwt.return_error e
  | Ok () ->
    match Binary.Writer.concat [Ok bytes; Ok signature_bytes] with
    | Error e -> Lwt.return_error e
    | Ok s -> silent_inject ?base s

let remote_forge ?base ~branch ops =
  let input = object method branch = branch method contents = ops end in
  let|>? h = post1 ?base ~input ~msg:"forge" Services.forge branch in
  Crypto.hex_to_raw h

type 'p op_more = {
  op : 'p manager_operation;
  bytes : Crypto.Raw.t;
  size : int;
  size_limits : int;
  limits0 : (int64 option * Z.t option * Z.t option);
}

type forge_kind =
  | LForge of script_expr op_more list
  | RForge of (script_expr op_more list * Crypto.Raw.t)

let make_op_more op limits0 =
  let n = op.man_numbers in
  let size_limits =
    Binary.Size.uint64 n.fee + Binary.Size.uzarith n.gas_limit +
    Binary.Size.uzarith n.storage_limit in
  {op; size_limits; size = 0; bytes = Crypto.Raw.mk ""; limits0}

let update_limits ?fee ?gas_limit ?storage_limit op =
  let n = op.man_numbers in
  let diff_size =
      (Option.fold ~none:0 ~some:(fun f -> Binary.Size.uint64 f - Binary.Size.uint64 n.fee) fee) +
      (Option.fold ~none:0 ~some:(fun g -> Binary.Size.uzarith g - Binary.Size.uzarith n.gas_limit) gas_limit) +
      (Option.fold ~none:0 ~some:(fun s -> Binary.Size.uzarith s - Binary.Size.uzarith n.storage_limit) storage_limit) in
  let fee = Option.value ~default:n.fee fee in
  let gas_limit = Option.value ~default:n.gas_limit gas_limit in
  let storage_limit = Option.value ~default:n.storage_limit storage_limit in
  { op with man_numbers = {n with fee; gas_limit; storage_limit} }, diff_size

let update_op_more_limits ?fee ?gas_limit ?storage_limit op_more =
  let n = op_more.op.man_numbers in
  let diff_size =
      (Option.fold ~none:0 ~some:(fun f -> Binary.Size.uint64 f - Binary.Size.uint64 n.fee) fee) +
      (Option.fold ~none:0 ~some:(fun g -> Binary.Size.uzarith g - Binary.Size.uzarith n.gas_limit) gas_limit) +
      (Option.fold ~none:0 ~some:(fun s -> Binary.Size.uzarith s - Binary.Size.uzarith n.storage_limit) storage_limit) in
  let fee = Option.value ~default:n.fee fee in
  let gas_limit = Option.value ~default:n.gas_limit gas_limit in
  let storage_limit = Option.value ~default:n.storage_limit storage_limit in
  let op = { op_more.op with man_numbers = {n with fee; gas_limit; storage_limit} } in
  {op_more with op; size = op_more.size + diff_size;
                size_limits = op_more.size_limits + diff_size}

let update_op_more_counters counter ops_more =
  let _counter, ops =
    List.fold_left (fun (counter, acc) op_more ->
        Z.succ counter,
        { op_more with op = { op_more.op with man_numbers = { op_more.op.man_numbers with counter } } } :: acc)
      (Z.succ counter, []) ops_more in
  List.rev ops

let update_op_more_bytes (bytes : Crypto.Raw.t) op_more =
  let size = String.length (bytes :> string) in
  {op_more with bytes; size}

let get_op_more {op;_} = op
let get_size_more {size; _} = size
let get_op_bytes_more {bytes; _} = bytes
let get_ops_more ops = List.map get_op_more ops
let get_sizes_more ops = List.map get_size_more ops
let get_ops_bytes_more ops = List.map get_op_bytes_more ops

let compare_with_remote ?base ~branch ops local_bytes =
  let>? remote_bytes = remote_forge ?base ~branch ops in
  if local_bytes <> remote_bytes then
    Lwt.return_error (`unexpected_operation_bytes (Crypto.hex_of_raw local_bytes, Crypto.hex_of_raw remote_bytes))
  else
    Lwt.return_ok local_bytes

let sign_operation ~sign bytes =
  let|>? signature = sign bytes in
  Binary.Writer.concat [bytes; signature]

let sign_zero_operation bytes =
  Lwt.return @@ Binary.Writer.concat [bytes; Ok (Crypto.Raw.mk @@ String.make 64 '\000')]

let forge_activation ?base ~pkh secret =
  let|>? header = get_header ?base () in
  let head = header#hash in
  let ops_byte = Forge.forge_activation ~pkh ~secret in
  Forge.forge_operations_base head [ ops_byte ]

let minimal_fees = Z.of_int 100
let nanotz_per_gas_unit = Z.of_int 100
let nanotz_per_byte = Z.of_int 1000
let to_nanotz m = Z.mul (Z.of_int 1000) m
let of_nanotz n = Z.div (Z.add (Z.of_int 999) n) (Z.of_int 1000)

let compute_fees ~gas_limit ~size =
  let minimal_fees_in_nanotz = to_nanotz minimal_fees in
  let fees_for_gas_in_nanotz =
    Z.mul nanotz_per_gas_unit gas_limit in
  let fees_for_size_in_nanotz = Z.mul nanotz_per_byte (Z.of_int size) in
  let fees_in_nanotz =
    Z.add minimal_fees_in_nanotz @@
    Z.add fees_for_gas_in_nanotz fees_for_size_in_nanotz in
  of_nanotz fees_in_nanotz

let iter_compute_fees ?(first=false) op n =
  let fee_start = Z.of_int64 op.op.man_numbers.fee in
  let gas_limit = op.op.man_numbers.gas_limit in
  let size_start = get_size_more op + (if first then 96 else 0) in
  let diff_size fee = Binary.Size.uzarith fee - Binary.Size.uzarith fee_start in
  let rec aux size i =
    if i = 0 then compute_fees ~gas_limit ~size
    else
      let fee = compute_fees ~gas_limit ~size in
      let diff_size = diff_size fee in
      let size = size_start + diff_size in
      aux size (i-1) in
  let fee = Z.to_int64 @@ aux size_start n in
  update_op_more_limits ~fee op

let check_original_limits ?gas_limit ?storage_limit op =
  let _, gas0, storage0 = op.limits0 in
  let gas_limit = match gas_limit, gas0 with
    | None, _ -> Ok None
    | gas, None -> Ok gas
    | Some gas, Some gas0 when gas0 >= gas -> Ok (Some gas0)
    | Some gas, Some _ ->
      Error (`prepare_operation_error (
          Format.sprintf "Gas limit low, operation will fail (minimum %s uxtz)" (Z.to_string gas))) in
  let storage_limit = match storage_limit, storage0 with
    | None, _ -> Ok None
    | storage, None -> Ok storage
    | Some storage, Some storage0 when storage0 >= storage -> Ok (Some storage0)
    | Some storage, Some _ ->
      Error (`prepare_operation_error (
          Format.sprintf "Storage limit low, operation will fail (minimum %s uxtz)" (Z.to_string storage))) in
  match gas_limit, storage_limit with
  | Ok gas_limit, Ok storage_limit ->
    Ok (update_op_more_limits ?gas_limit ?storage_limit op)
  | Error (`node_error l1), Error (`node_error l2) -> Error (`node_error (l1 @ l2))
  | Error e, _ | _, Error e -> Error e

let check_original_fees op =
  let fee0, _, _ = op.limits0 in
  let fee = op.op.man_numbers.fee in
  let fee = match fee0 with
    | None -> Ok None
    | Some fee0 when fee0 >= fee -> Ok (Some fee0)
    | _ ->
      Error (`prepare_operation_error (
          Format.sprintf "Fee too low, operation will never be included (minimum %Ld uxtz)" fee)) in
  match fee with
  | Ok fee -> Ok (update_op_more_limits ?fee op)
  | Error e -> Error e

let update_gas_storage_fee ?(first=false) op res =
  let is_reveal = match res.man_info.kind with Reveal _ -> true | _ -> false in
  let rec aux om io =
    if om.op_status <> `applied then
      let in_errors = List.flatten @@ List.map (fun i -> i.in_result.op_errors) io in
      Error (`node_error (om.op_errors @ in_errors))
    else
      let q, r = Z.(ediv_rem om.op_consumed_milligas (of_int 1000)) in
      let q = if r > Z.of_int 500 then Z.succ q else q in
      let consumed_gas = Z.(add (of_int (if is_reveal then 0 else 100)) q) in
      let consumed_storage = om.op_storage_size in
      let allocated = if om.op_allocated_destination_contract then 1 else 0 in
      let allocated = allocated + List.length om.op_originated_contracts in
      let consumed_storage =
        Z.add (Z.mul (Z.of_int allocated) (Z.of_int 257)) consumed_storage in
      let|$ l = List.fold_left (fun acc i ->
          match acc, aux i.in_result [] with
          | Error (`node_error l1), Error (`node_error l2) -> Error (`node_error (l1 @ l2))
          | Error e, _ | _, Error e -> Error e
          | Ok acc, Ok (g, s) -> Ok ((g, s) :: acc)) (Ok []) io in
      let l_gas, l_storage = List.split l in
      let gas = List.fold_left Z.add consumed_gas l_gas in
      let storage = List.fold_left Z.add consumed_storage l_storage in
      (gas, storage) in
  match res.man_metadata with
  | None -> Error `no_metadata
  | Some meta ->
    let$ (gas_limit, storage_limit) =
      aux meta.man_operation_result meta.man_internal_operation_results in
    let op = update_op_more_limits ~gas_limit ~storage_limit op in
    let$ op = check_original_limits ~gas_limit ~storage_limit op in
    let op = iter_compute_fees ~first op 1 in
    check_original_fees op

let run_operations ?(remove_failed=false) ?base ~branch ~chain_id ops =
  if not remove_failed then
    let|>? ops = run_operation ?base ~branch ~chain_id ops in
    ops, []
  else
    let|> (_, _, errored, ops) = Lwt_list.fold_left_s (fun (i, acc, errored, acc_res) op ->
        let op = { op with man_numbers = {
            op.man_numbers with counter = Z.(sub op.man_numbers.counter (of_int (List.length errored))) } } in
        let ops = acc @ [ op ] in
        let|> r = run_operation ?base ~branch ~chain_id ops in
        match r with
        | Ok res ->
          if List.for_all (function
              | {man_metadata = Some {man_operation_result = {op_status = `applied; _}; _}; _} -> true
              | _ -> false) res then
            i+1, ops, errored, res
          else
            let errors = List.flatten @@ List.map (fun m ->
                match m.man_metadata with
                | None -> []
                | Some m ->
                  let i_errors = List.flatten @@ List.map (fun m -> m.in_result.op_errors) m.man_internal_operation_results in
                  m.man_operation_result.op_errors @ i_errors) res in
            Format.eprintf "Operation %d failed:\n%s@." i (string_of_error (`node_error errors));
            i+1, acc, errored @ [ i ], acc_res
        | Error e ->
          Format.eprintf "Operation %d failed:\n%s@." i (string_of_error e);
          i+1, acc, errored @ [ i ], acc_res
      ) (0, [], [], []) ops in
    if ops = [] then Error (`prepare_operation_error "No succeeding operation left")
    else Ok (ops, errored)

let forge_auto_fees ?remove_failed ?(local_forge=true) ?base ~branch ~chain_id ops =
  let>? ops_res, errored = run_operations ?remove_failed ?base ~branch ~chain_id (get_ops_more ops) in
  let _, _, l = List.fold_left (fun (i, j, acc) op ->
      if List.mem i errored then i+1, j, acc
      else
        let op = { op with op = { op.op with man_numbers = { op.op.man_numbers with counter = Z.sub op.op.man_numbers.counter (Z.of_int (i-j)) } } } in
        i+1, j+1, acc @ [ op, List.nth ops_res j ]) (0, 0, []) ops in
  let>? ops = Lwt.return @@ snd @@
    List.fold_left (fun (first, acc) (op, res) ->
        match acc, update_gas_storage_fee ~first op res with
        | Error (`node_error l1), Error (`node_error l2) -> false, Error (`node_error (l1 @ l2))
        | Error e, _ | _, Error e -> false, Error e
        | Ok acc, Ok x -> false, Ok (x :: acc)) (true, Ok []) l in
  let ops = List.rev ops in
  if local_forge then
    let|>? ops_bytes = map (fun op -> Lwt.return (Forge.forge_operation op)) (get_ops_more ops) in
    LForge (List.map2 update_op_more_bytes ops_bytes ops), errored
  else
    let|>? ops_bytes = remote_forge ?base ~branch (get_ops_more ops) in
    RForge (ops, ops_bytes), errored

let prepare_ops ?base ~src ?(block="head") ops =
  let>? {ac_counter; _} = get_account_info ?base ~block src in
  let counter = Option.value ~default:Z.zero ac_counter in
  let ops = update_op_more_counters counter ops in
  let>? { hard_gas_limit_per_operation; hard_gas_limit_per_block; hard_storage_limit_per_operation; _ } =
    match config.constants with
    | Some constants -> Lwt.return_ok constants
    | None -> get_constants ?base () in
  let gas_limit =
    Z.min hard_gas_limit_per_operation
      (Z.div hard_gas_limit_per_block (Z.of_int (List.length ops))) in
  let ops =
    List.map
      (update_op_more_limits ~fee:0L ~gas_limit ~storage_limit:hard_storage_limit_per_operation)
      ops in
  let|>? ops_bytes = map (fun {op; _} -> Lwt.return @@ Forge.forge_operation op) ops in
  List.map2 update_op_more_bytes ops_bytes ops

let make_reveal ?base ?get_pk limits0 source ops =
  match get_pk with
  | None -> Lwt.return_ok (ops, limits0)
  | Some get_pk ->
    let>? r = get_manager_key ?base source in match r with
    | Some _ -> Lwt.return_ok (ops, limits0)
    | None ->
      let|>? pk = get_pk () in
      {
        man_info = { source; kind = Reveal pk };
        man_numbers = { fee = 0L; counter = Z.zero; gas_limit = Z.of_int 10000;
                        storage_limit = Z.zero };
        man_metadata = None } :: ops,
      (None, Some (Z.of_int 10000), Some Z.zero) :: limits0

let get_source = function
  | [] -> Error `empty_operation_list
  | { man_info = { source; _ }; _ } :: _ -> Ok source

let forge_manager_operations_base ?remove_failed ?local_forge ?base ?get_pk
    ?(block="head-2") ?(counter_block="head") ops =
  let>? src = Lwt.return @@ get_source ops in
  let limits0 = List.map (fun {man_numbers=n; _} ->
      (if n.fee = -1L then None else Some n.fee),
      (if n.gas_limit = Z.minus_one then None else Some n.gas_limit),
      (if n.storage_limit = Z.minus_one then None else Some n.storage_limit)) ops in
  let>? (ops, limits0) = make_reveal ?base ?get_pk limits0 src ops in
  let ops = List.map2 make_op_more ops limits0 in
  let>? header = get_header ?base ~block () in
  let>? ops = prepare_ops ?base ~block:counter_block ~src ops in
  let|>? r, errored = forge_auto_fees ?remove_failed ?local_forge ?base
      ~branch:header#hash ~chain_id:header#chain_id ops in
  r, header#hash, header#protocol, errored

let forge_manager_operations_select ?remove_failed ?(forge_method : forge_method =`both)
    ?base ?get_pk ?block ?counter_block ops =
  let>? ops = Lwt.return @@ unmacro_ops ops in
  let local_forge = match forge_method with `remote -> false | _ -> true in
  let>? r, branch, protocol, errored =
    forge_manager_operations_base ?remove_failed ~local_forge ?base ?get_pk
      ?block ?counter_block ops in
  match forge_method, r with
  | `remote, RForge (ops, bytes) ->
    Lwt.return_ok (bytes, protocol, branch, get_ops_more ops, errored)
  | `local, LForge ops ->
    begin match Forge.forge_operations branch (get_ops_more ops) with
      | Error e -> Lwt.return_error e
      | Ok bytes -> Lwt.return_ok (bytes, protocol, branch, get_ops_more ops, errored)
    end
  | `both, LForge ops ->
    begin match Forge.forge_operations branch (get_ops_more ops) with
      | Error e -> Lwt.return_error e
      | Ok bytes ->
        let|>? bytes = compare_with_remote ?base ~branch (get_ops_more ops) bytes in
        (bytes, protocol, branch, get_ops_more ops, errored)
    end
  | _ ->
    Lwt.return_error (`prepare_operation_error "Forge kinds not compatible")

let forge_manager_operations ?remove_failed ?forge_method ?base ?get_pk ?block
    ?counter_block ops =
  let|>? bytes, protocol, branch, ops, _ =
    forge_manager_operations_select ?remove_failed ?forge_method ?base ?get_pk ?block
      ?counter_block ops in
  bytes, protocol, branch, ops

let forge_empty_account ?base ~get_pk ~src (dst : A.contract) =
  let dst_s = (dst :> string) in
  if String.length dst_s < 2 || String.sub dst_s 0 2 <> "tz" then
    Lwt.return_error (`prepare_operation_error "Cannot empty an account to a KT1")
  else (
    let>? info = get_account_info ?base src in
    let tr = {
      man_info = {
        source = src;
        kind = Transaction {amount = info.ac_balance; destination = dst; parameters = None }
      };
      man_numbers = {fee=0L; gas_limit=Z.zero; storage_limit=Z.zero; counter=Z.zero};
      man_metadata = None} in
    let>? r, branch, protocol, _ =
      forge_manager_operations_base ?base ~get_pk [ tr ] in
    let ops = match r with RForge (ops, _) | LForge ops -> ops in
    let ops = get_ops_more ops in
    let fees, storage_limit = List.fold_left (fun (acc_f, acc_s) op ->
        Int64.add acc_f op.man_numbers.fee, Z.add acc_s op.man_numbers.storage_limit) (0L, Z.zero) ops in
    let burn = Int64.mul 1000L @@ Z.to_int64 storage_limit in
    let fix_amount tr =
      {tr with amount = Int64.(pred (sub tr.amount (add fees burn)))} in
    let ops = match ops with
      | [ rvl; {man_info = {kind = Transaction tr; _}; _} as op ] ->
        [rvl; {op with man_info = {op.man_info with kind = Transaction (fix_amount tr) } }]
      | [ {man_info = {kind = Transaction tr; _}; _} as op ] ->
        [ {op with man_info = {op.man_info with kind = Transaction (fix_amount tr) } } ]
      | l -> l in
    match Forge.forge_operations branch ops with
    | Error e -> Lwt.return_error e
    | Ok bytes -> Lwt.return_ok (bytes, ops, branch, protocol))
