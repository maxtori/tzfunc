open Proto
open Rp

let manager_kt =
  Micheline (Mseq [prim `parameter ~args:[prim `or_ ~args:[prim `lambda ~args:[prim `unit; prim `list ~args:[prim `operation]] ~annots:["%do"]; prim `unit ~annots:["%default"]]]; prim `storage ~args:[prim `key_hash]; prim `code ~args:[Mseq [Mseq [Mseq [prim `DUP; prim `CAR; prim `DIP ~args:[Mseq [prim `CDR]]]]; prim `IF_LEFT ~args:[Mseq [prim `PUSH ~args:[prim `mutez; Mint Z.zero]; prim `AMOUNT; Mseq [Mseq [prim `COMPARE; prim `EQ]; prim `IF ~args:[Mseq []; Mseq [Mseq [prim `UNIT; prim `FAILWITH]]]]; Mseq [prim `DIP ~args:[Mseq [prim `DUP]]; prim `SWAP]; prim `IMPLICIT_ACCOUNT; prim `ADDRESS; prim `SENDER; Mseq [Mseq [prim `COMPARE; prim `EQ]; prim `IF ~args:[Mseq []; Mseq [Mseq [prim `UNIT; prim `FAILWITH]]]]; prim `UNIT; prim `EXEC; prim `PAIR]; Mseq [prim `DROP; prim `NIL ~args:[prim `operation]; prim `PAIR]]]]])

let manager_kt_delegation_param = function
  | None ->
    Micheline (Mseq [
        prim `DROP; prim `NIL ~args:[prim `operation]; prim `None ~args:[prim `key_hash];
        prim `SET_DELEGATE; prim `CONS ])
  | Some delegate ->
    Micheline (Mseq [
        prim `DROP; prim ~args:[prim `operation] `NIL;
        prim ~args:[prim `key_hash; Mstring delegate] `PUSH;
        prim `SET_DELEGATE; prim `CONS ])

let manager_kt_transaction_param ?base (dst : A.contract) amount (ep, param) =
  let dst_s = ( dst :> string) in
  match Crypto.Prefix.pkh dst with
  | pref when pref = Some Crypto.Prefix.contract_public_key_hash ->
    let>? (ep, pty) = match ep with
      | None | Some "default" | Some "" -> Lwt.return_ok ("", prim `unit)
      | Some s ->
        let|>? ty = Node.get_entrypoint_type ?base dst s in
        "%" ^ s, ty in
    let|>? p = match param with
      | Some (Micheline m) -> Lwt.return_ok m
      | Some _ -> Lwt.return_error `unknown_language
      | None -> Lwt.return_ok @@ prim `Unit in
    Micheline (Mseq [
        prim `DROP;
        prim `NIL ~args:[prim `operation];
        prim `PUSH ~args:[prim `address; Mstring dst_s];
        prim `CONTRACT ~args:[pty] ~annots:[ep];
        prim (`macro "ASSERT_SOME");
        prim `PUSH ~args:[prim `mutez ~args:[Mint amount]];
        prim `PUSH ~args:[pty; p];
        prim `TRANSFER_TOKENS; prim `CONS ])
  | _ ->
    match param with
    | Some _ -> Lwt.return_error (`prepare_operation_error "no parameter from manager account")
    | _ ->
      Lwt.return_ok @@ Micheline (Mseq [
          prim `DROP;
          prim `NIL ~args:[prim `operation];
          prim `PUSH ~args:[prim `key_hash; Mstring dst_s];
          prim `IMPLICIT_ACCOUNT;
          prim `PUSH ~args:[prim `mutez; Mint amount];
          prim `UNIT; prim `TRANSFER_TOKENS; prim `CONS ])
