(*****************************************************************************)
(*                                                                           *)
(* MIT License                                                               *)
(*                                                                           *)
(* Copyright (c) 2021 Functori - contact@functori.com                        *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining     *)
(* a copy of this software and associated documentation files (the           *)
(* "Software"), to deal in the Software without restriction, including       *)
(* without limitation the rights to use, copy, modify, merge, publish,       *)
(* distribute, sublicense, and/or sell copies of the Software, and to        *)
(* permit persons to whom the Software is furnished to do so, subject to     *)
(* the following conditions:                                                 *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be            *)
(* included in all copies or substantial portions of the Software.           *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,           *)
(* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF        *)
(* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND                     *)
(* NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE    *)
(* LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION    *)
(* OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION     *)
(* WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.           *)
(*                                                                           *)
(*****************************************************************************)

open Proto

let (let>) = Lwt.bind
let (let|>) p f = Lwt.map f p
let (let>?) p f = Lwt.bind p (function Error e -> Lwt.return_error e | Ok x -> f x)
let (let|>?) p f = Lwt.map (Result.map f) p
let (let$) = Result.bind
let (let|$) p f = Result.map f p

let rec iter f = function
  | [] -> Lwt.return_ok ()
  | h :: t ->
      Lwt.bind (f h) @@ function
      | Error e -> Lwt.return_error e
      | Ok () -> iter f t

let rec fold f acc = function
  | [] -> Lwt.return_ok acc
  | h :: t ->
      Lwt.bind (f acc h) @@ function
      | Error e -> Lwt.return_error e
      | Ok acc -> fold f acc t

let map f l =
  let|>? l = fold (fun acc x -> let|>? x = f x in x :: acc) [] l in
  List.rev l

let cb p f =
  EzLwtSys.run @@ fun () ->
  Lwt.map (fun x -> match f with None -> () | Some f -> f x) p

let cbr ?error p f =
  EzLwtSys.run @@ fun () ->
  Lwt.map (fun r -> match f, error, r with
      | Some f, _, Ok x -> f x
      | _, Some f, Error e -> f e
      | _ -> ()) p

type slot_location = Printexc.location = {
  filename : string;
  line_number : int;
  start_char : int;
  end_char : int;
} [@@deriving encoding, jsoo]

type info_exn = {
  exn_id : int;
  exn_name : string;
  exn_content : string;
  exn_location : slot_location option; [@opt]
} [@@deriving encoding, jsoo]

let info_exn exn =
  let exn_id = Printexc.exn_slot_id exn in
  let exn_name = Printexc.exn_slot_name exn in
  let exn_content = Printexc.to_string exn in
  let callstack = Printexc.get_callstack 0 in
  let slots = Printexc.backtrace_slots callstack in
  let exn_location = match slots with
    | Some a -> begin try Printexc.Slot.location a.(0) with _ -> None end
    | None -> None in
  { exn_id; exn_name; exn_content; exn_location }

let exn_enc =
  Encoding.conv info_exn (fun {exn_name; _} -> failwith exn_name) info_exn_enc

type nonrec exn =
  exn
  [@class_type info_exn_jsoo]
  [@conv ((fun exn -> info_exn_to_jsoo @@ info_exn exn),
          (fun js -> failwith (Ezjs_min.to_string js##.name)))]
[@@deriving encoding, jsoo]

type nonrec error = [
  | `forge_error of (string [@wrap "msg"]) [@kind]
  | `unexpected_operation_hash [@kind]
  | `no_metadata [@kind]
  | `http_error of ((int [@key "code"]) * (string option [@key "content"]) [@object]) [@kind]
  | `node_error of (Proto.node_error list [@wrap "errors"]) [@kind]
  | `unexpected_operation_bytes of ((Proto.hex [@key "local"]) * (Proto.hex [@key "remote"]) [@object]) [@kind]
  | `prepare_operation_error of (string [@wrap "msg"]) [@kind]
  | `unknown_language [@kind]
  | `not_a_bool of ((char [@key "char"]) * (int [@key "position"]) [@object]) [@kind]
  | `int_too_long [@kind]
  | `unexpected_tag of ((string [@key "msg"]) * (int [@key "tag"]) [@object]) [@kind]
  | `no_case_matched of (int option [@wrap "tag"]) [@kind]
  | `recursive_error [@kind]
  | `wrong_michelson_type [@kind]
  | `unknown_primitive of (string option [@wrap "primitive"]) [@kind]
  | `unknown_micheline_tag [@kind]
  | `sign_exn of exn [@kind]
  | `empty_bytes [@kind]
  | `unknown_curve [@kind]
  | `string_too_short [@kind]
  | `unknown_prefix [@kind]
  | `invalid_b58 of exn [@kind]
  | `verify_exn of exn [@kind]
  | `empty_operation_list [@kind]
  | `unexpected_michelson [@kind]
  | `unmacro_error [@kind]
  | `generic of ((string [@key "name"]) * (string [@key "message"]) [@object])
  | `destruct_exn of exn [@kind]
  | `end_of_bytes of (int [@wrap "offset"]) [@kind]
  | `operation_not_handled [@kind]
] [@@deriving encoding, jsoo {modules=[Proto, Proto_jsoo]}]

let string_of_error e =
  EzEncoding.construct ~compact:false error_enc.Encoding.json e

let print_error e =
  Format.eprintf "\027[0;31mError:\027[0m\n%s@." (string_of_error e)
