open Proto
open Binary.Reader

let read_primitive s =
  let| i, s = uint8 s in
  match List.nth_opt primitives i with
  | Some (_, p) -> Ok ((p :> primitive_or_macro), s)
  | None -> Error (`unknown_primitive None)

let read_annots s =
  elem (fun n s ->
      let| st, s = sub n s in
      Ok (String.split_on_char ' ' st, s)) s

let rec read_micheline s =
  let| tag, s = uint8 s in
  match tag with
  | 0 ->
    let| i, s = zarith s in
    Ok (Mint i, s)
  | 1 ->
    let| st, s = string s in
    Ok (Mstring st, s)
  | 10 ->
    let| h, s = hex s in
    Ok (Mbytes h, s)
  | 2 ->
    let| l, s = list read_micheline s in
    Ok (Mseq l, s)
  | 3 ->
    let| p, s = read_primitive s in
    Ok (prim p, s)
  | 4 ->
    let| p, s = read_primitive s in
    let| annots, s = read_annots s in
    Ok (prim ~annots p, s)
  | 5 ->
    let| p, s = read_primitive s in
    let| arg, s = read_micheline s in
    Ok (prim ~args:[arg] p, s)
  | 6 ->
    let| p, s = read_primitive s in
    let| arg, s = read_micheline s in
    let| annots, s = read_annots s in
    Ok (prim ~args:[arg] ~annots p, s)
  | 7 ->
    let| p, s = read_primitive s in
    let| arg1, s = read_micheline s in
    let| arg2, s = read_micheline s in
    Ok (prim ~args:[arg1; arg2] p, s)
  | 8 ->
    let| p, s = read_primitive s in
    let| arg1, s = read_micheline s in
    let| arg2, s = read_micheline s in
    let| annots, s = read_annots s in
    Ok (prim ~args:[arg1; arg2] ~annots p, s)
  | 9 ->
    let| p, s = read_primitive s in
    let| args, s = list read_micheline s in
    let| annots, s = read_annots s in
    Ok (prim ~args:args ~annots p, s)
  | _ -> Error `unknown_micheline_tag

let rec unpack_typ p s =
  match p with
  | Mprim { prim = `string; _}
  | Mprim { prim = `nat; _}
  | Mprim { prim = `int; _}
  | Mprim { prim = `bytes; _}
  | Mprim { prim = `bool; _}
  | Mprim { prim = `unit; _ }
  | Mprim { prim = `mutez; _ } -> read_micheline s
  | Mprim { prim = `list; args = [ p ]; _ }
  | Mprim { prim = `set; args = [ p ]; _ } ->
    let| tag, s = uint8 s in
    if tag <> 2 then Error `wrong_michelson_type
    else
      let| l, s = list (unpack_typ p) s in
      Ok (Mseq l, s)
  | Mprim { prim = `pair; args = [ p1; p2 ]; _ } ->
    let| tag, s = uint8 s in
    let| pr, s = read_primitive s in
    let| arg1, s = unpack_typ p1 s in
    let| arg2, s = unpack_typ p2 s in
    let| annots, s = if tag = 7 then Ok ([], s) else read_annots s in
    Ok (prim pr ~args:[ arg1; arg2 ] ~annots, s)
  | Mprim { prim = `pair; args = p1 :: p; _ } ->
    let| tag, s = uint8 s in
    let| pr, s = read_primitive s in
    let| arg1, s = unpack_typ p1 s in
    let| arg2, s = unpack_typ (Mprim {prim=`pair; args=p; annots=[]}) s in
    let| annots, s = if tag = 7 then Ok ([], s) else read_annots s in
    Ok (prim pr ~args:[ arg1; arg2 ] ~annots, s)
  | Mprim { prim = `option; args = [ p ]; _ } ->
    let| tag, s = uint8 s in
    let| pr, s = read_primitive s in
    let| args, s =
      if tag = 3 then Ok ([], s)
      else
        let| arg, s = unpack_typ p s  in
        Ok ([arg], s) in
    let| annots, s = if tag = 3 || tag = 5 then Ok ([], s) else read_annots s in
    Ok (prim pr ~args ~annots, s)
  | Mprim { prim = `or_; args = [ l; r ]; _ } ->
    let| tag, s = uint8 s in
    let| pr, s = read_primitive s in
    let| arg, s = match pr with
      | `Left -> unpack_typ l s
      | `Right -> unpack_typ r s
      | _ -> Error `wrong_michelson_type in
    let| annots, s = if tag = 5 then Ok ([], s) else read_annots s in
    Ok (prim pr ~args:[arg] ~annots, s)
  | Mprim { prim = `map; args = [ pk; pv ]; _ } ->
    let| tag, s = uint8 s in
    if tag <> 2 then Error `wrong_michelson_type
    else
      let| l, s = list (fun s ->
          let| tag, s = uint8 s in
          let| pr, s = read_primitive s in
          let| k, s = unpack_typ pk s in
          let| v, s = unpack_typ pv s in
          let| annots, s = if tag = 7 then Ok ([], s) else read_annots s in
          Ok (prim pr ~args:[k; v] ~annots, s)
        ) s in
      Ok (Mseq l, s)
  | Mprim { prim = `key; _ } ->
    let| _tag, s = uint8 s in
    let| pk, s = elem (fun _ s -> pk s) s in
    Ok (Mstring (pk :> string), s)
  | Mprim { prim = `key_hash; _ } ->
    let| _tag, s = uint8 s in
    let| pkh, s = elem (fun _ s -> pkh s) s in
    Ok (Mstring (pkh :> string), s)
  | Mprim { prim = `address; _ } ->
    let| _tag, s = uint8 s in
    let| pkh, s = elem (fun _ s -> contract s) s in
    Ok (Mstring (pkh :> string), s)
  | Mprim { prim = `timestamp; _ } ->
    let| _tag, s = uint8 s in
    let| i, s = zarith s in
    let st = A.cal_to_str (CalendarLib.Calendar.from_unixfloat (Z.to_float i)) in
    Ok (Mstring st, s)
  | Mprim { prim = `signature; _ } ->
    let| _tag, s = uint8 s in
    let| sign, s = elem (fun _ s -> signature s) s in
    Ok (Mstring (sign :> string), s)
  | Mprim { prim = `chain_id; _ } ->
    let| _tag, s = uint8 s in
    let| c, s = elem (fun _ s -> chain_id s) s in
    Ok (Mstring (c :> string), s)
  | _ -> Error `wrong_michelson_type

let unpack ?typ s =
  let s = from_raw s in
  let| tag, s = uint8 s in
  if tag <> 5 then Error `wrong_michelson_type
  else
    let| m, _ = match typ with
      | None -> read_micheline s
      | Some p -> unpack_typ p s in
    Ok m
