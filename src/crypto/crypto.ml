(*****************************************************************************)
(*                                                                           *)
(* MIT License                                                               *)
(*                                                                           *)
(* Copyright (c) 2021 Functori - contact@functori.com                        *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining     *)
(* a copy of this software and associated documentation files (the           *)
(* "Software"), to deal in the Software without restriction, including       *)
(* without limitation the rights to use, copy, modify, merge, publish,       *)
(* distribute, sublicense, and/or sell copies of the Software, and to        *)
(* permit persons to whom the Software is furnished to do so, subject to     *)
(* the following conditions:                                                 *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be            *)
(* included in all copies or substantial portions of the Software.           *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,           *)
(* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF        *)
(* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND                     *)
(* NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE    *)
(* LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION    *)
(* OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION     *)
(* WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.           *)
(*                                                                           *)
(*****************************************************************************)

module Pbkdf = Pbkdf
module Bip39 = Bip39
module Box = Box
module Base58 = Base58
module Prefix = Prefix
module Raw = Raw
module H = H

let coerce (s : Raw.t) = (s :> string)
let hex_of_raw (s : Raw.t) : H.t = H.mk @@ Hex.(show @@ of_string (s :> string))
let hex_to_raw (s : H.t) : Raw.t = Raw.mk @@ Hex.to_string @@ `Hex (s :> string)
let concat_raw (l : Raw.t list) =
  Raw.mk @@ String.concat "" @@ List.map (fun (s : Raw.t) -> (s :> string)) l

let blake2b_32 (l : Raw.t list) : Raw.t = Raw.mk @@ Hacl.blake2b (concat_raw l :> string)
let blake2b_20 (l : Raw.t list) : Raw.t = Raw.mk @@ Hacl.blake2b ~length:20 (concat_raw l :> string)

module Curve = struct
  type t = [
    | `ed25519
    | `secp256k1
    | `p256 ]

  type error = [ `empty_bytes | `unknown_curve | `string_too_short | `unknown_prefix ]

  let from_raw (b : Raw.t) : (t * Raw.t, [> error]) result =
    let s = (b :> string) in
    let n = String.length s in
    if n = 0 then Error `empty_bytes
    else
      let s2 = Raw.mk (String.sub s 1 (n-1)) in
      match String.get s 0 with
      | '\000' -> Ok (`ed25519, s2)
      | '\001' -> Ok (`secp256k1, s2)
      | '\002' -> Ok (`p256, s2)
      | _ -> Error `unknown_curve

  let from_b58 s : (t, [> error]) result =
    let n = String.length s in
    if n < 6 then Error `string_too_short
    else match String.sub s 0 3 with
      | "tz1" -> Ok `ed25519
      | "tz2" -> Ok `secp256k1
      | "tz3" -> Ok `p256
      | _ -> match String.sub s 0 4 with
        | "edpk" | "edsk" -> Ok `ed25519
        | "sppk" | "spsk" -> Ok `secp256k1
        | "p2pk" | "p2sk" -> Ok `p256
        | _ -> match String.sub s 0 5 with
          | "edsig" -> Ok `ed25519
          | "p2sig" -> Ok `p256
          | _ -> match String.sub s 0 6 with
            | "spsig1" ->  Ok `secp256k1
            | _ -> Error `unknown_prefix

  let to_raw : t -> Raw.t = function
    | `ed25519 -> Raw.mk "\000"
    | `secp256k1 -> Raw.mk "\001"
    | `p256 -> Raw.mk "\002"

end

let b58dec ?alphabet ~prefix s =
  Result.bind (Curve.from_b58 s) @@ fun c ->
  try Ok (Base58.decode ?alphabet ~prefix:(prefix c) s)
  with exn -> Error (`invalid_b58 exn)

module Pkh = struct
  module T : sig type t = private Raw.t val mk : Raw.t -> t end = struct
    type t = Raw.t
    let mk s = s
  end
  type t = T.t

  let prefix : Curve.t -> Raw.t = function
    | `ed25519 -> Prefix.ed25519_public_key_hash
    | `secp256k1 -> Prefix.secp256k1_public_key_hash
    | `p256 -> Prefix.p256_public_key_hash

  let b58enc ?alphabet ?(curve=`ed25519) (b : t) =
    Base58.encode ?alphabet ~prefix:(prefix curve) (b :> Raw.t)

  let b58dec ?alphabet s =
    Result.map T.mk (b58dec ?alphabet ~prefix s)
end

type pkh = Pkh.t

module Pk = struct
  module T : sig type t = private Raw.t val mk : Raw.t -> t end = struct
    type t = Raw.t
    let mk s = s
  end
  type t = T.t

  let prefix : Curve.t -> Raw.t = function
    | `ed25519 -> Prefix.ed25519_public_key
    | `secp256k1 -> Prefix.secp256k1_public_key
    | `p256 -> Prefix.p256_public_key

  let b58enc ?alphabet ?(curve=`ed25519) (b : t) =
    Base58.encode ?alphabet ~prefix:(prefix curve) (b :> Raw.t)

  let b58dec ?alphabet s =
    Result.map T.mk (b58dec ?alphabet ~prefix s)

  let hash (b : t) =
    Pkh.T.mk @@ blake2b_20 [(b :> Raw.t)]
end

type pk = Pk.t

module Sk = struct
  module T : sig type t = private Raw.t val mk : Raw.t -> t end = struct
    type t = Raw.t
    let mk s = s
  end
  type t = T.t

  let prefix : Curve.t -> Raw.t = function
    | `ed25519 -> Prefix.ed25519_seed
    | `secp256k1 -> Prefix.secp256k1_secret_key
    | `p256 -> Prefix.p256_secret_key

  let b58enc ?alphabet ?(curve=`ed25519) (b : t) =
    Base58.encode ?alphabet ~prefix:(prefix curve) (b :> Raw.t)

  let b58dec ?alphabet s =
    Result.map T.mk (b58dec ?alphabet ~prefix s)

  let to_public_key (b : t) =
    let pk = Hacl.(to_public @@ sk (b :> string)) in
    Pk.T.mk @@ Raw.mk (pk :> string)
end

type sk = Sk.t

module Signature = struct
  module T : sig type t = private Raw.t val mk : Raw.t -> t end = struct
    type t = Raw.t
    let mk s = s
  end
  type t = T.t

  let prefix : Curve.t -> Raw.t = function
    | `ed25519 -> Prefix.ed25519_signature
    | `secp256k1 -> Prefix.secp256k1_signature
    | `p256 -> Prefix.p256_signature

  let b58enc ?alphabet ?(curve=`ed25519) (b : t) =
    Base58.encode ?alphabet ~prefix:(prefix curve) (b :> Raw.t)

  let b58dec ?alphabet s =
    Result.map T.mk (b58dec ?alphabet ~prefix s)
end

type signature = Signature.t

module Watermark = struct
  let block = Raw.mk "\001"
  let endorsement = Raw.mk "\002"
  let generic = Raw.mk "\003"
  let empty = Raw.mk ""
end

module Ed25519 = struct
  let keypair () =
    let sk = Hacl.sk @@ String.init 32 (fun _ -> Char.chr @@ Random.int 256) in
    let pk = Hacl.to_public sk in
    Pk.T.mk @@ Raw.mk @@ (pk :> string), Sk.T.mk @@ Raw.mk @@ (sk :> string)

  let sk_from_seed edsk =
    Result.bind (Curve.from_b58 edsk) @@ function
    | `ed25519 ->
      Result.bind (Sk.b58dec edsk) @@ fun sk ->
      let pk = Sk.to_public_key sk in
      Ok (Base58.encode ~prefix:Prefix.ed25519_secret_key @@
          concat_raw [ (pk :> Raw.t); (sk :> Raw.t) ])
    | _ -> Error `unknown_curve

  let sk_to_seed edsk =
    let sk = (Base58.decode ~prefix:Prefix.ed25519_secret_key edsk :> string) in
    Base58.encode ~prefix:Prefix.ed25519_seed @@ Raw.mk @@ String.sub sk 32 32

  let sign_bytes_exn ?(watermark=Watermark.empty) ~(sk : sk) (bytes : Raw.t) =
    let msg = blake2b_32 [ watermark; bytes ] in
    let signature = Hacl.sign ~sk:(Hacl.sk (sk :> string)) (msg :> string) in
    Signature.T.mk @@ Raw.mk signature

  let sign_bytes ?watermark ~sk bytes =
    try Ok (sign_bytes_exn ?watermark ~sk bytes)
    with exn -> Error (`sign_exn exn)

  let sign ?watermark ~edsk bytes =
    Result.bind (Curve.from_b58 edsk) @@ function
    | `ed25519 ->
      Result.bind (Sk.b58dec edsk) @@ fun sk ->
      Result.bind (sign_bytes ?watermark ~sk bytes) @@ fun b ->
      Ok (Signature.b58enc ~curve:`ed25519 b)
    | _ -> Error `unknown_curve

  let verify_bytes ~(pk : pk) ~(signature : signature) ~(bytes : Raw.t) =
    let msg = (blake2b_32 [ bytes ] :> string) in
    let signature = (signature :> string) in
    try Ok (Hacl.verify ~pk:(Hacl.pk (pk :> string)) ~msg ~signature)
    with exn -> Error (`verify_exn exn)

  let verify ~edpk ~edsig ~bytes =
    Result.bind (Curve.from_b58 edpk) @@ function
    | `ed25519 ->
      begin Result.bind (Curve.from_b58 edsig) @@ function
        | `ed25519 ->
          Result.bind (Pk.b58dec edpk) @@ fun pk ->
          Result.bind (Signature.b58dec edsig) @@ fun signature ->
          verify_bytes ~pk ~signature ~bytes
        | _ -> Error `unknown_curve
      end
    | _ -> Error `unknown_curve
end

let pk_to_pkh hash =
  Result.bind (Curve.from_b58 hash) @@ fun curve ->
  Result.bind (Pk.b58dec hash) @@ fun b ->
  let b = Pk.hash b in
  Ok (Pkh.b58enc ~curve b)

let op_to_KT1 ?(index=0) hash =
  let rec aux acc nbytes i =
    if nbytes <= 0 then Raw.mk @@ String.concat "" acc
    else
      let c = String.make 1 @@ Char.chr (i land 0xff) in
      aux (c :: acc) (nbytes - 1) (i lsr 8) in
  let data = concat_raw [
    Base58.decode ~prefix:Prefix.operation_hash hash;
    aux [] 4 index
  ] in
  Base58.encode ~prefix:Prefix.contract_public_key_hash (blake2b_20 [ data ])

let check_pkh str =
  match Pkh.b58dec str with
  | Ok _ -> String.length str = 20
  | _ -> false
