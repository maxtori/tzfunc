(*****************************************************************************)
(*                                                                           *)
(* MIT License                                                               *)
(*                                                                           *)
(* Copyright (c) 2021 Functori - contact@functori.com                        *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining     *)
(* a copy of this software and associated documentation files (the           *)
(* "Software"), to deal in the Software without restriction, including       *)
(* without limitation the rights to use, copy, modify, merge, publish,       *)
(* distribute, sublicense, and/or sell copies of the Software, and to        *)
(* permit persons to whom the Software is furnished to do so, subject to     *)
(* the following conditions:                                                 *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be            *)
(* included in all copies or substantial portions of the Software.           *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,           *)
(* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF        *)
(* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND                     *)
(* NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE    *)
(* LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION    *)
(* OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION     *)
(* WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.           *)
(*                                                                           *)
(*****************************************************************************)

let xorbuf a b =
  let alen = String.length a in
  if String.length b <> alen then invalid_arg "xor: both buffers must be of same size";
  String.mapi (fun i c -> Char.(chr (code c lxor code (String.get b i)))) a

let cdiv x y =
  if y < 1 then raise Division_by_zero else
  if x > 0 then 1 + ((x - 1) / y) else 0 [@@inline]

let be i =
  let rec aux acc nbytes i =
    if nbytes <= 0 then String.concat "" acc
    else
      let c = String.make 1 @@ Char.chr (i land 0xff) in
      aux (c :: acc) (nbytes - 1) (i lsr 8) in
  aux [] 4 i

let pbkdf2 ~password ~salt ~count ~dk_len =
  if count <= 0 then invalid_arg "count must be a positive integer" ;
  if dk_len <= 0 then invalid_arg "derived key length must be a positive integer" ;
  let h_len = 64 in
  let l = cdiv dk_len h_len in
  let r = dk_len - (l - 1) * h_len in
  let block i =
    let rec f u xor = function
      | 0 -> xor
      | j -> let u = Hacl.sha512_hmac ~key:password u in
        f u (xorbuf xor u) (j - 1) in
    let u_1 = Hacl.sha512_hmac ~key:password (salt ^ be i) in
    f u_1 u_1 (count - 1) in
  let rec loop blocks = function
    | 0 -> blocks
    | i -> loop ((block i)::blocks) (i - 1) in
  String.concat "" (loop [String.sub (block l) 0 r] (l - 1))
