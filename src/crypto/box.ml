(*****************************************************************************)
(*                                                                           *)
(* MIT License                                                               *)
(*                                                                           *)
(* Copyright (c) 2021 Functori - contact@functori.com                        *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining     *)
(* a copy of this software and associated documentation files (the           *)
(* "Software"), to deal in the Software without restriction, including       *)
(* without limitation the rights to use, copy, modify, merge, publish,       *)
(* distribute, sublicense, and/or sell copies of the Software, and to        *)
(* permit persons to whom the Software is furnished to do so, subject to     *)
(* the following conditions:                                                 *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be            *)
(* included in all copies or substantial portions of the Software.           *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,           *)
(* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF        *)
(* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND                     *)
(* NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE    *)
(* LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION    *)
(* OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION     *)
(* WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.           *)
(*                                                                           *)
(*****************************************************************************)

open Hacl

let salt_len = 8
let nonce_len = 24
let nonce = String.make nonce_len '\x00'
let () = Random.self_init ()

let pbkdf ?(count=32768) ~salt password =
  Pbkdf.pbkdf2 ~count ~dk_len:32 ~salt ~password

let encrypt ?count ~password sk =
  let salt = String.init salt_len (fun _ -> Char.chr @@ Random.int 256) in
  let key = pbkdf ?count ~salt password in
  Option.map (fun c -> salt ^ c) @@ secretbox ~key ~nonce ~msg:sk

let decrypt ?count ~password esk =
  let salt = String.sub esk 0 salt_len in
  let cypher = String.sub esk salt_len (String.length esk - salt_len) in
  let key = pbkdf ?count ~salt password in
  open_secretbox ~key ~nonce ~cypher
