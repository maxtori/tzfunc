(*****************************************************************************)
(*                                                                           *)
(* MIT License                                                               *)
(*                                                                           *)
(* Copyright (c) 2021 Functori - contact@functori.com                        *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining     *)
(* a copy of this software and associated documentation files (the           *)
(* "Software"), to deal in the Software without restriction, including       *)
(* without limitation the rights to use, copy, modify, merge, publish,       *)
(* distribute, sublicense, and/or sell copies of the Software, and to        *)
(* permit persons to whom the Software is furnished to do so, subject to     *)
(* the following conditions:                                                 *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be            *)
(* included in all copies or substantial portions of the Software.           *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,           *)
(* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF        *)
(* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND                     *)
(* NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE    *)
(* LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION    *)
(* OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION     *)
(* WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.           *)
(*                                                                           *)
(*****************************************************************************)

module Json = Json_encoding
module Bin = Binary.Encoding

type 'a encoding = {
  json : 'a Json.encoding;
  bin : 'a Bin.encoding;
}

type 'a field = {
  json_f : 'a Json.field;
  bin_f : 'a Bin.field;
}

type 'a case = {
  json_c : 'a Json.case;
  bin_c : 'a Bin.case;
}

let unit = { json = Json.unit; bin = Bin.unit }
let empty = { json = Json.empty; bin = Bin.empty }
let constant s = { json = Json.constant s; bin = Bin.constant s }
let int = { json = Json.int; bin = Bin.int }
let int32 = { json = Json.int32; bin = Bin.int32 }
let int53 = { json = Json.int53; bin = Bin.int53 }
let bool = { json = Json.bool; bin = Bin.bool }
let string = { json = Json.string; bin = Bin.string }
let bytes = { json = Json.bytes; bin = Bin.bytes }
let option enc = { json = Json.option enc.json; bin = Bin.option enc.bin }
let list enc = { json = Json.list enc.json; bin = Bin.list enc.bin }
let array enc = { json = Json.array enc.json; bin = Bin.array enc.bin }
let string_enum l = { json = Json.string_enum l; bin = Bin.string_enum l }
let merge_objs enc1 enc2 = {
  json = Json.merge_objs enc1.json enc2.json;
  bin = Bin.merge_objs enc1.bin enc2.bin }
let merge_tups enc1 enc2 = {
  json = Json.merge_tups enc1.json enc2.json;
  bin = Bin.merge_tups enc1.bin enc2.bin }
let assoc enc = { json = Json.assoc enc.json; bin = Bin.assoc enc.bin }
let conv of_ to_ enc = {
  json = Json.conv of_ to_ enc.json;
  bin = Bin.conv of_ to_ enc.bin }
let mu name ?title ?description f = {
  json = Json.mu name ?title ?description (fun json ->
      (f {json; bin = Bin.mu name ?title ?description (fun x -> x)}).json) ;
  bin = Bin.mu name ?title ?description (fun bin ->
      (f {bin; json = Json.mu name ?title ?description (fun x -> x)}).bin) }
let def name ?title ?description enc = {
  json = Json.def name ?title ?description enc.json;
  bin = Bin.def name ?title ?description enc.bin }

let req ?title ?description s enc = {
  json_f = Json.req ?title ?description s enc.json;
  bin_f = Bin.req ?title ?description s enc.bin }

let opt ?title ?description s enc = {
  json_f = Json.opt ?title ?description s enc.json;
  bin_f = Bin.opt ?title ?description s enc.bin }

let dft ?title ?description ?construct s enc def = {
  json_f = Json.dft ?title ?description ?construct s enc.json def;
  bin_f = Bin.dft ?title ?description ?construct s enc.bin def }

let case ?title ?description enc proj inj = {
  json_c = Json.case ?title ?description enc.json proj inj;
  bin_c = Bin.case ?title ?description enc.bin proj inj
}

let union l = {
  json = Json.union @@ List.map (fun x -> x.json_c) l;
  bin = Bin.union @@ List.map (fun x -> x.bin_c) l
}

let obj1 f1 = {json=Json.obj1 f1.json_f; bin=Bin.obj1 f1.bin_f}
let obj2 f1 f2 = {json=Json.obj2 f1.json_f f2.json_f; bin=Bin.obj2 f1.bin_f f2.bin_f}
let obj3 f1 f2 f3 = {json=Json.obj3 f1.json_f f2.json_f f3.json_f; bin=Bin.obj3 f1.bin_f f2.bin_f f3.bin_f}
let obj4 f1 f2 f3 f4 = {
  json=Json.obj4 f1.json_f f2.json_f f3.json_f f4.json_f;
  bin=Bin.obj4 f1.bin_f f2.bin_f f3.bin_f f4.bin_f}
let obj5 f1 f2 f3 f4 f5 = {
  json=Json.obj5 f1.json_f f2.json_f f3.json_f f4.json_f f5.json_f;
  bin=Bin.obj5 f1.bin_f f2.bin_f f3.bin_f f4.bin_f f5.bin_f}
let obj6 f1 f2 f3 f4 f5 f6 = {
  json=Json.obj6 f1.json_f f2.json_f f3.json_f f4.json_f f5.json_f f6.json_f;
  bin=Bin.obj6 f1.bin_f f2.bin_f f3.bin_f f4.bin_f f5.bin_f f6.bin_f}
let obj7 f1 f2 f3 f4 f5 f6 f7 = {
  json=Json.obj7 f1.json_f f2.json_f f3.json_f f4.json_f f5.json_f f6.json_f f7.json_f;
  bin=Bin.obj7 f1.bin_f f2.bin_f f3.bin_f f4.bin_f f5.bin_f f6.bin_f f7.bin_f}
let obj8 f1 f2 f3 f4 f5 f6 f7 f8 = {
  json=Json.obj8 f1.json_f f2.json_f f3.json_f f4.json_f f5.json_f f6.json_f f7.json_f f8.json_f;
  bin=Bin.obj8 f1.bin_f f2.bin_f f3.bin_f f4.bin_f f5.bin_f f6.bin_f f7.bin_f f8.bin_f}
let obj9 f1 f2 f3 f4 f5 f6 f7 f8 f9 = {
  json=Json.obj9 f1.json_f f2.json_f f3.json_f f4.json_f f5.json_f f6.json_f f7.json_f f8.json_f f9.json_f;
  bin=Bin.obj9 f1.bin_f f2.bin_f f3.bin_f f4.bin_f f5.bin_f f6.bin_f f7.bin_f f8.bin_f f9.bin_f}
let obj10 f1 f2 f3 f4 f5 f6 f7 f8 f9 f10 = {
  json=Json.obj10 f1.json_f f2.json_f f3.json_f f4.json_f f5.json_f f6.json_f f7.json_f f8.json_f f9.json_f f10.json_f;
  bin=Bin.obj10 f1.bin_f f2.bin_f f3.bin_f f4.bin_f f5.bin_f f6.bin_f f7.bin_f f8.bin_f f9.bin_f f10.bin_f}

let tup1 f1 = {json=Json.tup1 f1.json; bin=Bin.tup1 f1.bin}
let tup2 f1 f2 = {json=Json.tup2 f1.json f2.json; bin=Bin.tup2 f1.bin f2.bin}
let tup3 f1 f2 f3 = {json=Json.tup3 f1.json f2.json f3.json; bin=Bin.tup3 f1.bin f2.bin f3.bin}
let tup4 f1 f2 f3 f4 = {
  json=Json.tup4 f1.json f2.json f3.json f4.json;
  bin=Bin.tup4 f1.bin f2.bin f3.bin f4.bin}
let tup5 f1 f2 f3 f4 f5 = {
  json=Json.tup5 f1.json f2.json f3.json f4.json f5.json;
  bin=Bin.tup5 f1.bin f2.bin f3.bin f4.bin f5.bin}
let tup6 f1 f2 f3 f4 f5 f6 = {
  json=Json.tup6 f1.json f2.json f3.json f4.json f5.json f6.json;
  bin=Bin.tup6 f1.bin f2.bin f3.bin f4.bin f5.bin f6.bin}
let tup7 f1 f2 f3 f4 f5 f6 f7 = {
  json=Json.tup7 f1.json f2.json f3.json f4.json f5.json f6.json f7.json;
  bin=Bin.tup7 f1.bin f2.bin f3.bin f4.bin f5.bin f6.bin f7.bin}
let tup8 f1 f2 f3 f4 f5 f6 f7 f8 = {
  json=Json.tup8 f1.json f2.json f3.json f4.json f5.json f6.json f7.json f8.json;
  bin=Bin.tup8 f1.bin f2.bin f3.bin f4.bin f5.bin f6.bin f7.bin f8.bin}
let tup9 f1 f2 f3 f4 f5 f6 f7 f8 f9 = {
  json=Json.tup9 f1.json f2.json f3.json f4.json f5.json f6.json f7.json f8.json f9.json;
  bin=Bin.tup9 f1.bin f2.bin f3.bin f4.bin f5.bin f6.bin f7.bin f8.bin f9.bin}
let tup10 f1 f2 f3 f4 f5 f6 f7 f8 f9 f10 = {
  json=Json.tup10 f1.json f2.json f3.json f4.json f5.json f6.json f7.json f8.json f9.json f10.json;
  bin=Bin.tup10 f1.bin f2.bin f3.bin f4.bin f5.bin f6.bin f7.bin f8.bin f9.bin f10.bin}

let any = { json = Json.any_ezjson_value;
            bin = Bin.conv Ezjsonm_interface.to_string Ezjsonm_interface.from_string Bin.string}

(* proto *)
let hex_json = Json.conv (fun (s : Crypto.H.t) -> (s :> string)) (fun s -> Crypto.H.mk s) Json.string
let hex = {json=hex_json; bin=Bin.hex}
let uint32_json = Json.ranged_int32 ~minimum:0l ~maximum:Int32.max_int "uint32"
let uint32 = {json=uint32_json; bin=Bin.int32}
let uint64_json =
  Json.conv Int64.to_string (fun s -> let i = Int64.of_string s in if i < 0L then failwith "negative uint64" else i) Json.string
let uint64 = {json=uint64_json; bin=Bin.uint64}
let uzarith_json =
  Json.conv Z.to_string (fun s -> let i = Z.of_string s in if i < Z.zero then failwith "negative uzarith" else i) Json.string
let uzarith = {json=uzarith_json; bin=Bin.uzarith}

let zarith = {json=Json.conv Z.to_string Z.of_string Json.string; bin=Bin.zarith}
let pkh = {json=Json.string; bin=Bin.pkh}
let contract = {json=Json.string; bin=Bin.contract}
let pk = {json=Json.string; bin=Bin.pk}
let block_hash = {json=Json.string; bin=Bin.block_hash}
let signature = {json=Json.string; bin=Bin.signature}
let script_expr_hash = {json=Json.string; bin=Bin.script_expr_hash}
let operations_hash = {json=Json.string; bin=Bin.operations_hash}
let operation_hash = {json=Json.string; bin=Bin.operation_hash}
let context_hash = {json=Json.string; bin=Bin.context_hash}
let nonce_hash = {json=Json.string; bin=Bin.nonce_hash}
let protocol_hash = {json=Json.string; bin=Bin.protocol_hash}
let chain_id = {json=Json.string; bin=Bin.chain_id}
let tx_rollup_hash = {json=Json.string; bin=Bin.tx_rollup_hash}
let tx_inbox_hash = {json=Json.string; bin=Bin.tx_inbox_hash}
let tx_message_result_hash = {json=Json.string; bin=Bin.tx_message_result_hash}
let tx_commitment_hash = {json=Json.string; bin=Bin.tx_commitment_hash}
let sc_rollup_hash = {json=Json.string; bin=Bin.sc_rollup_hash}
let sc_commitment_hash = {json=Json.string; bin=Bin.sc_commitment_hash}
let sc_state_hash = {json=Json.string; bin=Bin.sc_state_hash}
let l2_address = {json=Json.string; bin=Bin.l2_address}
let construct ?compact enc x = EzEncoding.construct ?compact enc.json x
let destruct enc s = EzEncoding.destruct_res enc.json s
let serialize enc x = Binary.Encoding.construct enc.bin x
let deserialize enc s = Result.map fst Binary.Encoding.(destruct enc.bin @@ read s)
