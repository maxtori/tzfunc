(*****************************************************************************)
(*                                                                           *)
(* MIT License                                                               *)
(*                                                                           *)
(* Copyright (c) 2021 Functori - contact@functori.com                        *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining     *)
(* a copy of this software and associated documentation files (the           *)
(* "Software"), to deal in the Software without restriction, including       *)
(* without limitation the rights to use, copy, modify, merge, publish,       *)
(* distribute, sublicense, and/or sell copies of the Software, and to        *)
(* permit persons to whom the Software is furnished to do so, subject to     *)
(* the following conditions:                                                 *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be            *)
(* included in all copies or substantial portions of the Software.           *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,           *)
(* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF        *)
(* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND                     *)
(* NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE    *)
(* LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION    *)
(* OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION     *)
(* WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.           *)
(*                                                                           *)
(*****************************************************************************)

open Crypto

let (let|) = Result.bind

type error = [ `forge_error of string ]

let mk = Raw.mk

let char c : (Raw.t, _) result = Ok (Raw.mk @@ String.make 1 c)
let uint8 i = char @@ Char.chr i
let bool b = if b then uint8 0xff else uint8 0
let concat (l : (Raw.t, _) result list) : (Raw.t, _) result =
  let rec aux acc = function
    | [] -> Ok (Raw.mk @@ String.concat "" @@ List.rev acc)
    | Error e :: _ -> Error e
    | (Ok (s : Raw.t)) :: t -> aux ((s :> string) :: acc) t in
  aux [] l
let chars l = concat (List.map char l)
let option f = function
  | None -> bool false
  | Some x -> concat [bool true; f x]

let be nbytes i =
  let rec aux acc nbytes i =
    if nbytes <= 0 then concat acc
    else
      let c = uint8 (i land 0xff) in
      aux (c :: acc) (nbytes - 1) (i lsr 8) in
  aux [] nbytes i

let be64 nbytes i =
  let rec aux acc nbytes i =
    if nbytes <= 0 then concat acc
    else
      let c = uint8 Int64.(to_int @@ logand i 0xffL) in
      aux (c :: acc) (nbytes - 1) @@ Int64.shift_right i 8 in
  aux [] nbytes i

let bez nbytes i =
  let rec aux acc nbytes i =
    if nbytes <= 0 then concat acc
    else
      let c = uint8 Z.(to_int @@ logand i @@ of_int 0xff) in
      aux (c :: acc) (nbytes - 1) @@ Z.shift_right i 8 in
  aux [] nbytes i

let int16 i = be 2 i
let int i = be 4 i
let int32 i = int (Int32.to_int i)
let int64 i = be64 8 i

let unit () = Ok (Raw.mk "")
let raw (r : Raw.t) = concat [int @@ String.length (r :> string); Ok r]
let string s = raw (Raw.mk s)
let hex h = raw (hex_to_raw h)

let bytes b = string (Bytes.to_string b)
let list f l = concat @@ List.map f l
let array f a = list f (Array.to_list a)
let any (_len : int) s : (Raw.t, _) result = Ok s

(* proto *)
let uint64 z =
  if z < 0L then Error (`forge_error "negative uint64")
  else
    let rec f acc x =
      if x < 0x80L then uint8 (Int64.to_int x) :: acc
      else f (uint8 Int64.(to_int (logor (logand x 0x7fL) 0x80L)) :: acc) (Int64.shift_right x 7) in
    concat (List.rev @@ f [] z)

let uzarith z =
  if z < Z.zero then Error (`forge_error "negative uzarith")
  else
    let rec f acc x =
      if x < Z.of_int 0x80 then uint8 (Z.to_int x) :: acc
      else f (uint8 Z.(to_int (logor (logand x (of_int 0x7f)) (of_int 0x80))) :: acc)
          (Z.shift_right x 7) in
    concat (List.rev @@ f [] z)

let zarith z =
  let sign = z < Z.zero in
  let small = Z.numbits z <= 6 in
  let z = Z.abs z in
  let first_byte = uint8 @@
    Z.to_int @@ Z.logor (Z.logand z (Z.of_int 0x3f)) @@
    Z.of_int @@ ((if sign then 0x40 else 0x00)
                 lor (if small then 0x00 else 0x80)) in
  if small then first_byte
  else concat [first_byte; uzarith @@ Z.shift_right z 6]

let elem = function
  | Error e -> Error e
  | Ok (s : Raw.t) -> raw s

let base58 ~prefix s =
  try Ok (Base58.decode ~prefix s) with _ -> Error (`forge_error "base58 error")

let pkh pkh = (* 21 bytes *)
  let pkh_s = (pkh :> string) in
  let| tag, prefix =
    if String.length pkh_s < 3 then Error (`forge_error (Format.sprintf "wrong format for pkh: %S" pkh_s))
    else match String.sub pkh_s 0 3 with
      | "tz1" -> Ok (0, Prefix.ed25519_public_key_hash)
      | "tz2" -> Ok (1, Prefix.secp256k1_public_key_hash)
      | "tz3" -> Ok (2, Prefix.p256_public_key_hash)
      | _ -> Error (`forge_error (Format.sprintf "wrong format for pkh: %S" pkh_s)) in
  concat [ uint8 tag; base58 ~prefix pkh ]

let contract h = (* 22 bytes *)
  let h_s = (h :> string) in
  if h_s.[0] = 'd' || h_s.[0] = 't' then concat [ uint8 0; pkh h]
  else
    concat [
      uint8 1;
      base58 ~prefix:Prefix.contract_public_key_hash h;
      uint8 0 ]

let pk pk = (* 33 or 34 bytes *)
  let pk_s = (pk :> string) in
  let| tag, prefix =
    if String.length pk_s < 4 then Error (`forge_error (Format.sprintf "wrong format for pk: %S" pk_s))
    else match String.sub pk_s 0 4 with
      | "edpk" -> Ok (0, Prefix.ed25519_public_key)
      | "sppk" -> Ok (1, Prefix.secp256k1_public_key)
      | "p2pk" -> Ok (2, Prefix.p256_public_key)
      | _ -> Error (`forge_error (Format.sprintf "wrong format for pkh: %S" pk_s)) in
  concat [ uint8 tag; base58 ~prefix pk ]

let signature s =
  let s_s = (s :> string) in
  let| prefix =
    if String.length s_s < 3 then Error (`forge_error (Format.sprintf "wrong format for signature: %S" s_s))
    else if String.sub s_s 0 3 = "sig" then Ok Prefix.generic_signature
    else (
      if String.length s_s < 5 then
        Error (`forge_error (Format.sprintf "wrong format for signature: %S" s_s))
      else (match String.sub s_s 0 5 with
          | "edsig" -> Ok Prefix.ed25519_signature
          | "spsig" -> Ok Prefix.secp256k1_signature
          | "p2sig" -> Ok Prefix.p256_signature
          | _ -> Error (`forge_error (Format.sprintf "wrong format for signature: %S" s_s)))) in
  base58 ~prefix s

let block_hash = base58 ~prefix:Prefix.block_hash
let script_expr_hash = base58 ~prefix:Prefix.script_expr_hash
let protocol_hash = base58 ~prefix:Prefix.protocol_hash
let chain_id = base58 ~prefix:Prefix.chain_id
let operations_hash = base58 ~prefix:Prefix.operation_list_list_hash
let operation_hash = base58 ~prefix:Prefix.operation_hash
let context_hash = base58 ~prefix:Prefix.context_hash
let nonce_hash = base58 ~prefix:Prefix.nonce_hash
let tx_rollup_hash = base58 ~prefix:Prefix.tx_rollup_hash
let tx_inbox_hash = base58 ~prefix:Prefix.tx_inbox_hash
let tx_message_result_hash = base58 ~prefix:Prefix.tx_message_result_hash
let tx_commitment_hash = base58 ~prefix:Prefix.tx_commitment_hash
let sc_rollup_hash = base58 ~prefix:Prefix.sc_rollup_hash
let sc_commitment_hash = base58 ~prefix:Prefix.sc_commitment_hash
let sc_state_hash = base58 ~prefix:Prefix.sc_state_hash
let l2_address = base58 ~prefix:Prefix.l2_address
