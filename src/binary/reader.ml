(*****************************************************************************)
(*                                                                           *)
(* MIT License                                                               *)
(*                                                                           *)
(* Copyright (c) 2021 Functori - contact@functori.com                        *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining     *)
(* a copy of this software and associated documentation files (the           *)
(* "Software"), to deal in the Software without restriction, including       *)
(* without limitation the rights to use, copy, modify, merge, publish,       *)
(* distribute, sublicense, and/or sell copies of the Software, and to        *)
(* permit persons to whom the Software is furnished to do so, subject to     *)
(* the following conditions:                                                 *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be            *)
(* included in all copies or substantial portions of the Software.           *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,           *)
(* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF        *)
(* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND                     *)
(* NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE    *)
(* LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION    *)
(* OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION     *)
(* WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.           *)
(*                                                                           *)
(*****************************************************************************)

open Crypto

let (let|) = Result.bind

type state = {s : Raw.t; offset: int}

type error = [
  |  `not_a_bool of (char * int)
  | `int_too_long
  | `unexpected_tag of (string * int)
  | `end_of_bytes of int
]

let from_string s = { s = Raw.mk s; offset = 0 }
let from_raw s = { s; offset = 0 }
let from_hex s = { s = hex_to_raw s; offset = 0 }
let from_hex_string s = { s = hex_to_raw (H.mk s); offset = 0 }
let read_string f s =
  let| x, _ = f (from_string s) in Ok x
let read_raw f s =
  let| x, _ = f (from_raw s) in Ok x
let read_hex f s =
  let| x, _ = f (from_hex s) in Ok x
let read_hex_string f s =
  let| x, _ = f (from_hex_string s) in Ok x

let char s =
  try Ok (String.get (s.s :> string) s.offset, {s with offset = s.offset + 1})
  with _ -> Error (`end_of_bytes s.offset)
let uint8 s =
  let| i, s = char s in
  Ok (int_of_char i, s)

let bool s = match char s with
  | Ok ('\000', s) -> Ok (false, s)
  | Ok ('\255', s) -> Ok (true, s)
  | Ok (c, _) -> Error (`not_a_bool (c, s.offset))
  | Error e -> Error e

let option f s = match bool s with
  | Ok (false, s) -> Ok (None, s)
  | Ok (true, s) ->
    let| x, s = f s in Ok (Some x, s)
  | Error e -> Error e

let be nbytes s =
  let rec aux acc nbytes s =
    if nbytes <= 0 then Ok (acc, s)
    else
      let| c, s = uint8 s in
      aux ((acc lsl 8) lor c) (nbytes-1) s in
  aux 0 nbytes s

let be64 ?limit nbytes s =
  let rec aux ?(limit=10) acc nbytes s =
    if limit <= 0 then Error `int_too_long
    else if nbytes <= 0 then Ok (acc, s)
    else
      let| c, s = uint8 s in
      aux ~limit:(limit-1) Int64.(logor (shift_left acc 8) (of_int c)) (nbytes-1) s in
  aux ?limit 0L nbytes s

let bez ?limit nbytes s =
  let rec aux ?(limit=10) acc nbytes s =
    if limit <= 0 then Error `int_too_long
    else if nbytes <= 0 then Ok (acc, s)
    else
      let| c, s = uint8 s in
      aux ~limit:(limit-1) Z.(logor (shift_left acc 8) (of_int c)) (nbytes-1) s in
  aux ?limit Z.zero nbytes s

let int s = be 4 s
let int32 s =
  let| i, s = int s in
  Ok (Int32.of_int i, s)
let int64 s = be64 8 s

let elem f s =
  let| len, s = int s in
  f len s

let sub len s =
  try Ok (String.sub (s.s :> string) s.offset len, {s with offset = s.offset + len})
  with _ -> Error (`end_of_bytes (s.offset + len))

let unit s = Ok ((), s)
let string s = elem sub s
let hex s =
  let| r, s = string s in
  Ok (hex_of_raw (Raw.mk r), s)

let bytes s =
  let| str, s = elem sub s in
  Ok (Bytes.of_string str, s)
let list f s =
  let f len s =
    let rec aux s l = function
      | 0 -> Ok (List.rev l, s)
      | i when i > 0 -> let offset = s.offset in
        let| x, s = f s in
        aux s (x :: l) (i - (s.offset - offset))
      | _ -> Error `int_too_long in
    aux s [] len in
  elem f s
let array f s =
  let| l, s = list f s in
  Ok (Array.of_list l, s)
let any len s =
  let| a, s = sub len s in
  Ok (Raw.mk a, s)

(* proto *)
let uint64 ?limit s =
  let rec f acc s i =
    match limit with
    | Some limit when i > limit -> Error `int_too_long
    | _ ->
      let| v, s = uint8 s in
      let acc = Int64.(add acc (shift_left (of_int @@ v land 0x7f) (7 * i))) in
      if v land 0x80 <> 0 then f acc s (i+1)
      else Ok (acc, s) in
  f 0L s 0

let uzarith ?limit s =
  let rec f acc s i =
    match limit with
    | Some limit when i > limit -> Error `int_too_long
    | _ ->
      let| v, s = uint8 s in
      let acc =
        Z.add acc (Z.shift_left (Z.of_int @@ v land 0x7f) (7 * i)) in
      if v land 0x80 <> 0 then f acc s (i+1)
      else Ok (acc, s) in
  f Z.zero s 0

let zarith ?limit s =
  let rec f sign acc s i =
    match limit with
    | Some limit when i > limit -> Error `int_too_long
    | _ ->
      let| v, s = uint8 s in
      let sign = if i = 0 then v land 0x40 <> 0 else sign in
      let acc =
        if i = 0 then Z.add acc (Z.of_int @@ v land 0x3f)
        else Z.add acc (Z.shift_left (Z.of_int @@ v land 0x7f) (6 + 7 * (i-1))) in
      if v land 0x80 <> 0 then f sign acc s (i+1)
      else Ok (sign, acc, s) in
  let| (sign, acc, s) = f true Z.zero s 0 in
  if sign then Ok (Z.neg acc, s) else Ok (acc, s)

let pkh s =
  let| tag, s = uint8 s in
  let| prefix = match tag with
    | 0 -> Ok Prefix.ed25519_public_key_hash
    | 1 -> Ok Prefix.secp256k1_public_key_hash
    | 2 -> Ok Prefix.p256_public_key_hash
    | n -> Error (`unexpected_tag ("pkh", n)) in
  let| b, s = sub 20 s in
  Ok (Base58.encode ~prefix @@ Raw.mk b, s)

let contract s =
  let| tag, s = uint8 s in
  match tag with
  | 0 -> pkh s
  | 1 ->
    let| b, s = sub 20 s in
    let| _, s = sub 1 s in
    Ok (Base58.encode ~prefix:Prefix.contract_public_key_hash @@ Raw.mk b, s)
  | n -> Error (`unexpected_tag ("contract", n))

let pk s =
  let| tag, s = uint8 s in
  let| prefix, len = match tag with
    | 0 -> Ok (Prefix.ed25519_public_key, 32)
    | 1 -> Ok (Prefix.secp256k1_public_key, 33)
    | 2 -> Ok (Prefix.p256_public_key, 33)
    | n -> Error (`unexpected_tag ("pk", n)) in
  let| b, s = sub len s in
  Ok (Base58.encode ~prefix @@ Raw.mk b, s)

let b58 n prefix s =
  let| b, s = sub n s in
  Ok (Base58.encode ~prefix @@ Raw.mk b, s)

let block_hash = b58 32 Prefix.block_hash
let signature = b58 64 Prefix.generic_signature
let script_expr_hash = b58 32 Prefix.script_expr_hash
let operations_hash = b58 32 Prefix.operation_list_list_hash
let operation_hash = b58 32 Prefix.operation_hash
let context_hash = b58 32 Prefix.context_hash
let nonce_hash = b58 32 Prefix.nonce_hash
let protocol_hash = b58 32 Prefix.protocol_hash
let chain_id = b58 4 Prefix.chain_id
let tx_rollup_hash = b58 20 Prefix.tx_rollup_hash
let tx_inbox_hash = b58 32 Prefix.tx_inbox_hash
let tx_message_result_hash = b58 32 Prefix.tx_message_result_hash
let tx_commitment_hash = b58 32 Prefix.tx_commitment_hash
let sc_rollup_hash = b58 20 Prefix.sc_rollup_hash
let sc_commitment_hash = b58 32 Prefix.sc_commitment_hash
let sc_state_hash = b58 32 Prefix.sc_state_hash
let l2_address = b58 20 Prefix.l2_address
