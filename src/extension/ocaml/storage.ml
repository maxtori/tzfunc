open Chrome_lwt
open Ezjs_min
open Tzfunc.Rp
open Etypes

type storage_approved = string list [@@deriving jsoo]

let get_accounts () =
  let|> o = Storage.get ~key:"accounts" sync in
  Option.value ~default:[] @@ to_optdef (to_listf account_of_jsoo) o##.accounts

let get_approved () =
  let|> o = Storage.get ~key:"approved" local in
  Option.value ~default:[] @@ to_optdef storage_approved_of_jsoo o##.approved

let base_of_network = function
  | "mainnet" -> Ok {n_url = EzAPI.BASE "https://tz.functori.com"; n_name = Some "mainnet"}
  | "ghostnet" -> Ok {n_url = EzAPI.BASE "https://ghost.tz.functori.com"; n_name = Some "ghostnet"}
  | "jakartanet" -> Ok {n_url = EzAPI.BASE "https://jakarta.tz.functori.com"; n_name = Some "jakartanet"}
  | s ->
    if String.length s >= 4 && String.sub s 0 4 = "http" then Ok {n_url=EzAPI.BASE s; n_name=None}
    else Error (`generic ("unknown network", s))

let get_network () =
  let|> o = Storage.get ~key:"network" local in
  let s = Option.value ~default:"mainnet" (to_optdef to_string o##.network) in
  base_of_network s

let get_selected () =
  let|> o = Storage.get ~key:"selected" local in
  to_optdef to_string o##.selected

let get_info ?name ?network accounts =
  let>? i_network = match network with
    | Some network -> Lwt.return @@ base_of_network network
    | None -> get_network () in
  match name with
  | None ->
    let> s = get_selected () in
    begin match s with
      | None -> Lwt.return_ok {i_account=List.hd accounts; i_network}
      | Some s ->
        match List.find_opt (function {a_tz1; _} when a_tz1 = s -> true | _ -> false) accounts with
        | None -> Lwt.return_ok {i_account=List.hd accounts; i_network}
        | Some i_account -> Lwt.return_ok {i_account; i_network}
    end
  | Some s ->
    match List.find_opt (function {Etypes.a_tz1; _} when a_tz1 = s -> true | _ -> false) accounts with
    | None -> Lwt.return_error (`generic ("unknown account", s))
    | Some i_account -> Lwt.return_ok {i_account; i_network}

let add_account a =
  let> accounts = get_accounts () in
  let accounts = accounts @ [ a ] in
  let|> _ = Storage.set sync (object%js
      val accounts = of_listf Etypes.account_to_jsoo accounts
    end) in
  accounts

let update_account a =
  let> accounts = get_accounts () in
  let accounts = List.map (fun ac -> if ac.a_tz1 = a.a_tz1 then a else ac) accounts in
  let|> _ = Storage.set sync (object%js
      val accounts = of_listf Etypes.account_to_jsoo accounts
    end) in
  accounts

let remove_account a =
  let> accounts = get_accounts () in
  let accounts = List.filter (fun ac -> if ac.a_tz1 = a.a_tz1 then false else true) accounts in
  let|> _ = Storage.set sync (object%js
      val accounts = of_listf Etypes.account_to_jsoo accounts
    end) in
  accounts

let set_approved a =
  let> l = get_approved () in
  let|> _ = Storage.set local (object%js
      val approved = of_listf string @@ l @ [ a.Jext_lwt.Types.url ]
    end) in
  ()

let set_network network =
  let|> _ = Storage.set local (object%js val network = string network end) in
  base_of_network network

let set_selected account =
  let|> _ = Storage.set local (object%js val selected = string account.a_tz1 end) in
  ()
