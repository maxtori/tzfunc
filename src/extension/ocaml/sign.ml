open Tzfunc
open Ezjs_min
open Rp
open Ext
open Etypes
open State

type result =
  | SRsignature of string
  | SRerror of Rp.error
[@@deriving jsoo {modules=[Rp, Rp_jsoo]}]

class type data = object
  method result : result_jsoo t optdef prop
  method processing: bool t prop
end

type props = {
  req: sign_request;
  id: int;
  info: logged
} [@@deriving jsoo]

class type all = object
  inherit data
  inherit props_jsoo
end

let send_req app r =
  let|> _ = Common.(send_req ~id:app##.id @@ mk_request r) in
  app##.processing := _false

let sign_aux (app : all t) =
  let p = props_of_jsoo (app :> props_jsoo t) in
  let> o = get_password () in match o with
  | None ->
    let e = `generic ("unlogged", "") in
    app##.result := def @@ result_to_jsoo @@ SRerror e;
    send_req app (Rq_callback_error e)
  | Some password ->
    let> r = Common.get_sign ?watermark:p.req.sir_watermark ~password p.info.l_account in
    match r with
    | Error e ->
      app##.result := def @@ result_to_jsoo @@ SRerror e;
      let|> _ = Common.send_req ~id:p.id
          {Etypes.req=Rq_callback_error e; account=None; network=None} in
          app##.processing := _false
    | Ok f ->
      let> r = f p.req.sir_bytes in
      match r with
      | Error e ->
        app##.result := def @@ result_to_jsoo @@ SRerror e;
        send_req app (Rq_callback_error e)
      | Ok s ->
        let s = Crypto.Signature.(b58enc @@ T.mk s) in
        app##.result := def @@ result_to_jsoo @@ SRsignature s;
        send_req app (Rq_callback_ok (Rs_sign s))

let sign app =
  app##.processing := _true;
  EzLwtSys.run (fun () -> let> () = EzLwtSys.sleep 0.02 in sign_aux app)

let unpacked app =
  let sir = sign_request_of_jsoo app##.req in
  match sir.sir_watermark with
  | None -> undefined
  | Some w ->
    try
      match Read.unpack ?typ:sir.sir_type @@ Crypto.concat_raw [w; sir.sir_bytes] with
      | Error _ -> undefined
      | Ok m -> def (Proto_jsoo.micheline_to_jsoo m)
    with _ -> undefined

module C = Vue_component.Make(struct
    type nonrec data = data
    type nonrec all = all
    let name = "v-sign"
    let element = Vue_component.CRender (Render.sign_render, Render.sign_static_renders)
    let props = Some (Vue_component.PrsArray ["req"; "id"; "info"])
  end)

let init () =
  C.add_method0 "sign" sign;
  C.add_computed "unpacked" unpacked;
  let data = object%js
    val mutable result = undefined
    val mutable processing = _false
  end in
  C.make ~data:(fun _ -> data) ()
