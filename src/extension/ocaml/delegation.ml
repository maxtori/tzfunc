open Ezjs_min
open Ext.Etypes
open Tzfunc.Rp
open Operation

class type data = object
  method delegate : js_string t prop
  method fee: Tz.jsoo t optdef prop
  method gas_limit_: js_string t prop
  method storage_limit_: js_string t prop
  method counter: js_string t prop
  method visible: bool t prop
end

class type props = object
  method info : State.logged_jsoo t readonly_prop
end

class type all = object
  inherit data
  inherit props
end

let forge (app : all t) =
  let account = account_of_jsoo app##.info##.account in
  let delegate = match to_string app##.delegate with
    | "" | "none" | "null" | "undelegate" -> None
    | s -> Some s in
  let fee = Option.map Tz.abs @@ to_optdef Tz.of_jsoo app##.fee in
  let gas_limit, storage_limit, counter =
    to_string app##.gas_limit_, to_string app##.storage_limit_, to_string app##.counter in
  let gas_limit = if gas_limit = "" then None else try Some (Z.of_string gas_limit) with _ -> None in
  let storage_limit = if storage_limit = "" then None else try Some (Z.of_string storage_limit) with _ -> None in
  let counter = if counter = "" then None else try Some (Z.of_string counter) with _ -> None in
  let ops = [ Tzfunc.Utils.delegation ~source:account.a_tz1 ?fee ?gas_limit ?storage_limit ?counter delegate ] in
  let|> r = Operation.forge_aux ~account ops in
  let f = match r with
    | Error e -> Ferror e
    | Ok res -> Finfo res in
  emit_forged app f

let path app p _ =
  match to_string p with
  | "reset" -> app##.delegate := string ""
  | _ -> ()

let update_fee app tz = app##.fee := tz

module C = Vue_component.Make(struct
    type nonrec data = data
    type nonrec all = all
    let name = "v-delegation"
    let element = Vue_component.CRender (Render.delegation_render, Render.delegation_static_renders)
    let props = Some (Vue_component.PrsArray ["info"])
  end)

let init () =
  C.add_method0 "forge" forge;
  C.add_method1 "update_fee" update_fee;
  C.add_watch "path" path;
  let data _ = object%js
    val mutable delegate = string ""
    val mutable fee = undefined
    val mutable gas_limit_ = string ""
    val mutable storage_limit_ = string ""
    val mutable counter = string ""
    val mutable visible = _false
  end in
  C.make ~data ()
