open Ezjs_min
open Ext.Etypes
open Tzfunc
open Rp

type forge_info = {
  fr_bytes: raw;
  fr_protocol: string;
  fr_branch: string;
  fr_proto_operations: Proto.script_expr Proto.manager_operation list;
  fr_operations: operation_request list
} [@@deriving jsoo {modules=[Proto, Proto_jsoo]}]

type forge =
  | Finput
  | Frequest of send_request [@mutable]
  | Finfo of forge_info [@mutable]
  | Ferror of Rp.error
[@@deriving jsoo {modules=[Rp, Rp_jsoo]}]

type inject =
  | Ierror of Rp.error
  | Ihash of string
[@@deriving jsoo {modules=[Rp, Rp_jsoo]}]

class type data = object
  method forge_result_: forge_jsoo t prop
  method inject_result_: inject_jsoo t optdef prop
  method processing: bool t prop
  method copy_message_ : js_string t prop
end

type props = {
  req : send_request option;
  id : int option;
  info : State.logged;
  path : string;
} [@@deriving jsoo]

class type all = object
  inherit data
  inherit props_jsoo
end

let forge_aux ~account ?remove_failed ?forge_method ops =
  let get_pk () = Lwt.return_ok account.a_edpk in
  let|>? fr_bytes, fr_protocol, fr_branch, fr_proto_operations =
    Node.forge_manager_operations ~get_pk ?remove_failed ?forge_method ops in
  let fr_operations = List.map Ext.to_operation_request fr_proto_operations in
  { fr_bytes; fr_protocol; fr_branch; fr_operations; fr_proto_operations }

let forge_remove_undefined r =
  let f a = a##.operations##forEach (wrap_callback (fun x _ _ -> remove_undefined x)) in
  match Optdef.to_option r##.request, Optdef.to_option r##.info with
   | None, None -> ()
   | Some a, _ -> f a
   | _, Some a -> f a

let forge app =
  app##.processing := _true;
  EzLwtSys.run @@ fun () ->
  let> () = EzLwtSys.sleep 0.02 in
  match forge_of_jsoo app##.forge_result_ with
  | Frequest req ->
    let account = account_of_jsoo app##.info##.account in
    let ops = List.map (Ext.of_operation_request ~source:account.a_tz1) req.ser_operations in
    let|> r = forge_aux ~account ?remove_failed:req.ser_remove_failed ?forge_method:req.ser_forge_method ops in
    let f = match r with
      | Error e -> Ferror e
      | Ok i -> Finfo i in
    let r = forge_to_jsoo f in
    forge_remove_undefined r;
    app##.processing := _false;
    app##.forge_result_ := r
  | _ ->
    app##.processing := _false;
    Lwt.return_unit

let emit_forged app f =
  Vue_js.emit1 app "forged" (forge_to_jsoo f)

let set_forged app f =
  forge_remove_undefined f;
  app##.forge_result_ := f

let send_req app r =
  let|> _ = match Optdef.to_option app##.id with
    | None -> Lwt.return_ok None
    | Some id -> Common.(send_req ~id @@ mk_request r) in
  app##.processing := _false

let inject_aux app =
  let> o = Ext.get_password () in match o with
  | None ->
    let e = `generic ("unlogged", "") in
    app##.inject_result_ := def @@ inject_to_jsoo @@ Ierror e;
    send_req app (Rq_callback_error e)
  | Some password ->
    match forge_of_jsoo app##.forge_result_ with
    | Finfo r ->
      let account = account_of_jsoo app##.info##.account in
      let> rs = Common.get_sign ~watermark:Crypto.Watermark.generic ~password account in
      begin match rs with
        | Error e ->
          app##.inject_result_ := def @@ inject_to_jsoo @@ Ierror e;
          app##.processing := _false;
          send_req app (Rq_callback_error e)
        | Ok sign ->
          let> r2 = Node.inject ~bytes:r.fr_bytes ~branch:r.fr_branch
              ~protocol:r.fr_protocol ~sign r.fr_proto_operations in
          match r2 with
          | Ok hash ->
            app##.inject_result_ := def @@ inject_to_jsoo @@ Ihash hash;
            let rs = Rs_send {
                hash; branch=r.fr_branch; protocol=r.fr_protocol;
                bytes=r.fr_bytes; operations=r.fr_operations } in
            app##.processing := _false;
            send_req app (Rq_callback_ok rs)
          | Error e ->
            app##.inject_result_ := def @@ inject_to_jsoo @@ Ierror e;
            app##.processing := _false;
            send_req app (Rq_callback_error e)
      end
    | _ ->
      app##.processing := _false;
      Lwt.return_unit

let inject (app : all t) =
  app##.processing := _true;
  EzLwtSys.(run (fun () -> let> () = sleep 0.02 in inject_aux app))

let cancel app =
  app##.inject_result_ := undefined;
  app##.forge_result_ := forge_to_jsoo Finput

let path app p _ =
  match to_string p with
  | "reset" -> cancel app
  | _ -> ()

let forge_req r =
  let f = match to_optdef send_request_of_jsoo r with
    | Some req -> Frequest req
    | _ -> Finput in
  let r = forge_to_jsoo f in
  forge_remove_undefined r;
  r

let req app r _ =
  let r = forge_req r in
  app##.forge_result_ := r

let explorer_url app =
  match to_optdef to_string app##.info##.network##.name with
  | Some "mainnet" -> def (string "https://tzkt.io/")
  | Some "ghostnet" -> def (string "https://ghostnet.tzkt.io/")
  | Some "jakartanet" -> def (string "https://jakartanet.tzkt.io/")
  | _ ->
    log "no explorer for network: %S" (to_string app##.info##.network##.url);
    undefined

module C = Vue_component.Make(struct
    type nonrec data = data
    type nonrec all = all
    let name = "v-operation"
    let element = Vue_component.CRender (Render.operation_render, Render.operation_static_renders)
    let props = Some (Vue_component.PrsArray ["req"; "id"; "info"; "path"])
  end)

let init () =
  C.add_method0 "inject" inject;
  C.add_method1 "copy" Common.copy;
  C.add_method1 "set_forged" set_forged;
  C.add_method0 "cancel" cancel;
  C.add_method0 "forge" forge;
  C.add_watch "path" path;
  C.add_watch "req" req;
  C.add_computed "explorer_url" explorer_url;
  let data app =
    let r = forge_req app##.req in
    object%js
      val mutable inject_result_ = undefined
      val mutable forge_result_ = r
      val mutable processing = _false
      val mutable copy_message_ = string "copy"
  end in
  C.make ~data ()
