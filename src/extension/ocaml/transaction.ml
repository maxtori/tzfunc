open Ezjs_min
open Ext.Etypes
open Tzfunc.Rp
open Operation

type micheline_input = {
  mi_input: Mtyped.ftype;
  mi_output: (Proto.micheline [@conv (Proto_jsoo.micheline_to_jsoo_aux ~remove_args:false, Proto_jsoo.micheline_of_jsoo)]);
} [@@deriving jsoo {modules=[Proto, Proto_jsoo; Mtyped, Mtyped_jsoo]}]

type transaction_info = {
  ti_amount: Tz.t; [@mutable]
  ti_destination: string;
  ti_entrypoints: (string * micheline_input) list; [@mutable] [@assoc]
  ti_entrypoint_select: string;
  ti_entrypoint_input: string;
  ti_parameters: string;
  ti_fee: Tz.t option; [@mutable]
  ti_gas_limit: string;
  ti_storage_limit: string;
  ti_counter: string;
} [@@deriving jsoo {modules=[Proto, Proto_jsoo]}]

class type data = object
  method tr : transaction_info_jsoo t prop
  method processing : bool t prop
  method visible : bool t prop
end

class type props = object
  method info : State.logged_jsoo t readonly_prop
  method path : js_string t readonly_prop
  method request : send_request_jsoo t optdef readonly_prop
end

class type all = object
  inherit data
  inherit props
end

let empty = {
  ti_amount = { Tz.value=0L; unit = "ꜩ" }; ti_destination = "";
  ti_entrypoint_select = ""; ti_entrypoints = []; ti_entrypoint_input = "";
  ti_parameters = ""; ti_fee = None; ti_gas_limit = ""; ti_storage_limit = "";
  ti_counter = "";
}

let string_of_entrypoint =
  let open Proto in function
    | EPdefault -> "default"
    | EProot -> "root"
    | EPdo -> "do"
    | EPset -> "set"
    | EPremove -> "remove"
    | EPnamed s -> s

let get_entrypoints contract =
  let|>? l = Tzfunc.Node.get_entrypoints contract in
  List.filter_map (fun (k, v) ->
      match Mtyped.parse_type v with
      | Error _ -> None
      | Ok mi_input ->
        let s = Mtyped.short mi_input in
        try Some (k, {mi_input; mi_output = Micheline.empty_micheline_value s})
        with _ -> None) l

let fill_transaction app =
  match to_optdef send_request_of_jsoo app##.request with
  | Some {ser_operations = [ { kind = Rtransaction tr; _ } as req ] ; _} ->
    let ti_destination = tr.Proto.destination in
    let ti_fee, ti_gas_limit, ti_storage_limit, ti_counter =
      Option.map (fun value -> { Tz.value; unit = "μꜩ" }) req.fee,
      Option.fold ~none:"" ~some:Z.to_string req.gas_limit,
      Option.fold ~none:"" ~some:Z.to_string req.storage_limit,
      Option.fold ~none:"" ~some:Z.to_string req.counter in
    if String.length ti_destination = 36 && String.sub ti_destination 0 3 = "KT1" then
      let|> r = get_entrypoints ti_destination in
      begin match r with
        | Error e -> emit_forged app (Ferror e); empty
        | Ok l ->
          let ti_entrypoints, ti_entrypoint_select = match tr.Proto.parameters with
            | None -> l, ""
            | Some p ->
              let select = string_of_entrypoint p.Proto.entrypoint in
              match p.Proto.value with
              | Proto.Bytes _ -> l, select
              | Proto.Micheline m ->
                match List.assoc_opt select l with
                | None ->
                  let msg = Format.sprintf "entrypoint %S does not exist for contract %S"
                      select ti_destination in
                  emit_forged app (Ferror (`generic ("contract_error", msg)));
                  l, ""
                | Some mi ->
                  let l = List.remove_assoc select l in
                  (select, {mi with mi_output = m}) :: l, select  in
          { empty with ti_amount = {Tz.value = tr.Proto.amount; unit = "μꜩ"}; ti_destination;
                       ti_entrypoints; ti_entrypoint_select; ti_fee; ti_gas_limit;
                       ti_storage_limit; ti_counter }
      end
    else
      Lwt.return {
        empty with ti_amount = {Tz.value = tr.Proto.amount; unit = "μꜩ"}; ti_destination;
                   ti_fee; ti_gas_limit; ti_storage_limit; ti_counter}
  | _ -> Lwt.return empty

let load_entrypoints (app : all t) =
  let|> r = get_entrypoints @@ to_string app##.tr##.destination in
  match r with
  | Error e -> emit_forged app (Ferror e)
  | Ok l ->
    let x = (Table.makef micheline_input_to_jsoo l) in
    app##.tr##.entrypoints := x

let forge app =
  app##.processing := _true;
  let> () = EzLwtSys.sleep 0.02 in
  let ti = transaction_info_of_jsoo app##.tr in
  if ti.ti_destination = "" then (
    emit_forged app (Ferror (`forge_error "empty destination"));
    app##.processing := _false;
    Lwt.return_unit)
  else
    let amount = Tz.abs ti.ti_amount in
    let entrypoint = match ti.ti_entrypoint_select, ti.ti_entrypoint_input with
      | "", _ | "custom", "" -> None
      | "custom", s -> Some s
      | s, _ -> Some s in
    let param = match entrypoint with
      | None -> None
      | Some e -> match List.assoc_opt e ti.ti_entrypoints, ti.ti_parameters with
        | None, "" -> None
        | None, s -> Some (EzEncoding.destruct Proto.script_expr_enc.Proto.json s)
        | Some v, _ -> Some (Proto.Micheline v.mi_output) in
    let account = account_of_jsoo app##.info##.account in
    let fee = Option.map Tz.abs ti.ti_fee in
    let gas_limit = if ti.ti_gas_limit = "" then None else try Some (Z.of_string ti.ti_gas_limit) with _ -> None in
    let storage_limit = if ti.ti_storage_limit = "" then None else try Some (Z.of_string ti.ti_storage_limit) with _ -> None in
    let counter = if ti.ti_counter = "" then None else try Some (Z.of_string ti.ti_counter) with _ -> None in
    let ops = [ Tzfunc.Utils.transaction ~source:account.a_tz1 ~amount ?fee
                  ?gas_limit ?storage_limit ?counter ?entrypoint ?param ti.ti_destination ] in
    let|> r = Operation.forge_aux ~account ops in
    let f = match r with
      | Error e -> Ferror e
      | Ok res -> Finfo res in
    app##.processing := _false;
    emit_forged app f

let path app p _ =
  match to_string p with
  | "reset" -> app##.tr := transaction_info_to_jsoo empty
  | _ -> ()

let update_transaction app =
  EzLwtSys.run @@ fun () ->
  let|> tr = fill_transaction app in
  app##.tr := transaction_info_to_jsoo tr

let update_amount app tz = app##.tr##.amount := tz
let update_fee app tz = app##.tr##.fee := tz

module C = Vue_component.Make(struct
    type nonrec data = data
    type nonrec all = all
    let name = "v-transaction"
    let element = Vue_component.CRender (Render.transaction_render, Render.transaction_static_renders)
    let props = Some (Vue_component.PrsArray ["info"; "path"; "request"])
  end)

let init () =
  C.add_method0 "load_entrypoints" load_entrypoints;
  C.add_method0 "forge" forge;
  C.add_method1 "update_amount" update_amount;
  C.add_method1 "update_fee" update_fee;
  C.add_watch "path" path;
  let tr = transaction_info_to_jsoo empty in
  let data _ =
    object%js
      val mutable tr = tr
      val mutable processing = _false
      val mutable visible = _false
    end in
  C.make ~data ~lifecycle:["mounted", update_transaction] ()
