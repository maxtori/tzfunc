open Ezjs_min

type data = {
  file: string;
  path: string; [@mutable]
  locked: bool; [@mutable]
  info: State.logged option; [@mutable]
} [@@deriving jsoo {modules=[Proto, Proto_jsoo; Rp, Rp_jsoo]}]

module V = Vue_js.Make(struct
    type data = data_jsoo
    type all = data
    let id = "app"
  end)

let set_logged app l =
  app##.info := l;
  app##.path := string ""

let route app s =
  app##.path := s

let () =
  let vue_json_viewer = Unsafe.global##._VueJsonPretty in
  let vue = Unsafe.global##._Vue in
  ignore @@ vue##component (string "v-json") vue_json_viewer##.default;
  V.add_method1 "set_logged" set_logged;
  let _ = Sign.init () in
  let _ = Create.init () in
  let _ = Settings.init () in
  let _ = State.init () in
  let _ = Transaction.init () in
  let _ = Delegation.init () in
  let _ = Operation.init () in
  let _ = Header.init () in
  let _ = Create.init () in
  let _ = Micheline.init () in
  let _ = View.init () in
  let _ = Tz.init () in
  V.add_method1 "route" route;
  let file, _ = State.get_url_params () in
  let data = object%js
    val file = string file
    val mutable path = string ""
    val mutable locked = _true
    val mutable info = undefined
  end in
  let _app = V.init ~data ~export:true ~render:Render.app_render ~static_renders:Render.app_static_renders () in
  Hacl.ready (fun () -> ())
