open Ext
open Etypes
open Ezjs_min
open Tzfunc.Rp
include Jext_lwt.Client.Lib(Etypes)

let rrun f =
  EzLwtSys.run @@ fun () ->
  Lwt.map (Result.iter_error print_error) @@
  f ()

let runa0 f app =
  EzLwtSys.run (fun () -> f app)
let runa1 f app x =
  EzLwtSys.run (fun () -> f app x)

let mk_request req =
  {req; account=None; network=None}

let get_sk ~password account =
  let open Crypto in
  let esk = Base58.decode ~prefix:Prefix.ed25519_encrypted_seed account.a_edesk in
  match Box.decrypt ~count:4096 ~password (esk :> string) with
  | None -> Lwt.return_error (`generic ("decrypt_error", "cannot decrypt encrypted secret key with this password"))
  | Some sk -> Lwt.return_ok (Sk.T.mk @@ Raw.mk sk)

let get_sign ?watermark ~password account =
  let>? sk = get_sk ~password account in
  let sign b = match Crypto.Ed25519.sign_bytes ?watermark ~sk b with
    | Error e -> Lwt.return_error e
    | Ok b -> Lwt.return_ok (b :> Crypto.Raw.t) in
  Lwt.return_ok sign

let copy_aux ~value ~message app =
  let open EzLwtSys in
  ignore @@ Unsafe.global##.self##.navigator##.clipboard##writeText (value app);
  message app (string "copied!");
  run @@ fun () ->
  let|> () = sleep 2. in
  message app (string "copy")

let copy app s =
  copy_aux ~value:(fun _ -> s) ~message:(fun app s -> app##.copy_message_ := s) app

let emit_routed app route =
  Vue_js.emit1 app "routed" route

let copy_container =
  let copy = copy_aux ~value:(fun app -> app##.value) ~message:(fun app s -> app##.message := s) in
  let open Vue_component in
  make
    ~props:(PrsArray ["value"; "variant"])
    ~render:Render.copy_container_render
    ~static_renders:Render.copy_container_static_renders
    ~methods:Mjs.(L [ "copy", Unsafe.inject @@ wrap_meth_callback copy ])
    ~data:(fun _ -> object%js val message = string "copy" end)
    "v-copy"
