type t = {
  value: Proto.A.uint64; [@mutable]
  unit: string; [@mutable]
} [@@deriving jsoo {modules=[Proto, Proto_jsoo]}]

let enter app =
  Vue_js.emit0 app "entered"

let change_value app value =
  app##.value := value;
  if Ezjs_min.to_string value = "" then
    Vue_js.emit1 app "changed" Ezjs_min.undefined
  else
    Vue_js.emit1 app "changed" (object%js val value = value val unit = app##.unit end)

let change_unit app unit =
  app##.unit := unit;
  Vue_js.emit1 app "changed" (object%js val value = app##.value val unit = unit end)

let abs tz = if tz.unit = "ꜩ" then Int64.mul tz.value 1000000L else tz.value

module C = Vue_component.Make(struct
    class type data = jsoo
    class type all = object
      inherit jsoo
      method opt: bool Ezjs_min.t Ezjs_min.opt Ezjs_min.readonly_prop
    end
    let name = "v-tz"
    let element = Vue_component.CRender (Render.tz_render, Render.tz_static_renders)
    let props = Some (Vue_component.PrsArray ["size"; "opt"])
  end)

let init () =
  C.add_method0 "enter" enter;
  C.add_method1 "change_value" change_value;
  C.add_method1 "change_unit" change_unit;
  let data app =
    match Ezjs_min.to_opt Ezjs_min.to_bool app##.opt with
    | Some true ->
      object%js
        val mutable value = Ezjs_min.string ""
        val mutable unit = Ezjs_min.string "ꜩ"
      end
    | _ -> to_jsoo { value = 0L; unit = "ꜩ" } in
  C.make ~data ()
