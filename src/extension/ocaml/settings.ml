open Tzfunc
open Ezjs_min
open Rp
open Ext
open Etypes
open State

class type data = object
  method network_select_ : js_string t prop
  method network_input_ : js_string t prop
  method copy_message_ : js_string t prop
  method password : js_string t prop
  method processing : bool t prop
end

class type props = object
  method info : logged_jsoo t readonly_prop
  method file : js_string t readonly_prop
  method path : js_string t readonly_prop
end

class type all = object
  inherit data
  inherit props
end

module C = Vue_component.Make(struct
    type nonrec data = data
    type nonrec all = all
    let name = "v-settings"
    let element = Vue_component.CRender (Render.settings_render, Render.settings_static_renders)
    let props = Some (Vue_component.PrsArray ["info"; "path"; "file"])
  end)

let update_info app =
  let account = account_of_jsoo app##.info##.account in
  let network = network_of_jsoo app##.info##.network in
  let|> logged = get_logged ~account ~network false in
  State.emit_logged app (Some logged)

let update_account (app : all t) (a : account_jsoo t) =
  let account = account_of_jsoo a in
  let> accounts = Storage.update_account account in
  let network = network_of_jsoo app##.info##.network in
  let|> logged = get_logged ~accounts ~account ~network false in
  State.emit_logged app (Some logged)

let remove_account (app : all t) (a : account_jsoo t) =
  let> accounts = Storage.remove_account (account_of_jsoo a) in
  let> r = Storage.get_info accounts in match r with
  | Error _ -> Lwt.return_unit
  | Ok {i_account; i_network} ->
    let|> logged = get_logged ~accounts ~account:i_account ~network:i_network false in
    State.emit_logged app (Some logged)

let set_network app =
  let accounts = to_listf account_of_jsoo app##.info##.accounts in
  let account = account_of_jsoo app##.info##.account in
  set_network ~accounts ~account app

let show_secret app account =
  app##.processing := _true;
  EzLwtSys.run @@ fun () ->
  let> () = EzLwtSys.sleep 0.02 in
  let a = account_of_jsoo account in
  let password = to_string app##.password in
  app##.password := string "";
  let> r = Common.get_sk ~password a in
  let () = match r with
    | Error _ -> ()
    | Ok sk -> (Unsafe.coerce account)##.edsk := string (Crypto.Sk.b58enc sk) in
  app##.processing := _false;
  Lwt.return_unit

let path app p _ =
  match to_string p with
  | "reset" ->
    app##.network_select_ := string "";
    app##.network_input_ := string "";
    app##.password := string "";
    app##.processing := _false
  | _ -> ()

let init () =
  C.add_method1 "set_network" set_network;
  C.add_method1 "copy" Common.copy;
  C.add_method1 "update_account" (Common.runa1 update_account);
  C.add_method1 "remove_account" (Common.runa1 remove_account);
  C.add_method0 "update_info" (Common.runa0 update_info);
  C.add_method1 "show_secret" show_secret;
  C.add_watch "path" path;
  let data _ = object%js
    val mutable network_select_ = string ""
    val mutable network_input_ = string ""
    val mutable copy_message_ = string "copy"
    val mutable password = string ""
    val mutable processing = _false
  end in
  C.make ~data ()
