open Ezjs_min
open Ext
open Etypes
open Tzfunc
open Rp

type logged = {
  l_account: account; [@mutable]
  l_network: network; [@mutable]
  l_accounts: account list;
  l_node: Proto.account option;
  l_sign_request: sign_request option;
  l_send_request: send_request option;
  l_request_id: int option;
  l_request_account: bool;
  l_request_network: bool;
} [@@deriving jsoo {modules=[Proto, Proto_jsoo]}]

type state =
  | Blank [@key "blank"]
  | Enable [@key "enable"]
  | Approve of Jext_lwt.Types.site_metadata [@key "approve"]
  | Unlock [@key "unlock"]
  | Info [@key "info"]
  | State_error of {name:string; message:string} [@key "error"]
[@@deriving jsoo]

class type data = object
  method state: state_jsoo t prop
  method password: js_string t prop
  method file: js_string t readonly_prop
  method id: int optdef readonly_prop
  method req: request_jsoo t optdef readonly_prop
  method processing: bool t prop
end

class type props = object
  method path: js_string t readonly_prop
end

class type all = object
  inherit data
  inherit props
end

let get_url_params () =
  let url = to_string Dom_html.window##.location##.href in
  match String.rindex_opt url '/' with
  | None -> "", []
  | Some j ->
    let name = String.sub url (j+1) (String.length url - j - 1) in
    match String.rindex_opt name '?' with
    | None -> Filename.remove_extension name, []
    | Some i ->
      let file = String.sub name 0 i in
      let s = String.sub name (i+1) (String.length name - i - 1) in
      Filename.remove_extension file, List.filter_map (fun s -> match String.split_on_char '=' s with
          | [ k; v] -> Some (k, v)
          | _ -> None) @@ String.split_on_char '&' s

let get_req_info () =
  let file, params = get_url_params () in
  let id = match List.assoc_opt "id" params with
    | None -> None
    | Some id -> try Some (int_of_string id) with _ -> None in
  let req = match List.assoc_opt "req" params with
    | None -> None
    | Some s ->
      try
        let r = Jext_lwt.Common.decode s in
        Some (request_of_jsoo r)
      with _ -> None in
  file, id, req

let get_logged ?id ?req ?accounts ~network ~account update =
  let _, id, req =
    if update then get_req_info ()
    else "", id, req in
  let> accounts = match accounts with
    | None -> Storage.get_accounts ()
    | Some accounts -> Lwt.return accounts in
  let EzAPI.BASE url = network.n_url in
  Tzfunc.Node.set_node url;
  let> _ = Tzfunc.Node.set_constants () in
  let> r = Tzfunc.Node.get_account_info account.a_tz1 in
  let l_node = Result.to_option r in
  let|> l_sign_request, l_send_request, l_request_account, l_request_network = match req with
    | Some ({ Etypes.req = Rq_permission { req; _ }; account; network } as r) ->
      let req_account = Option.is_some account in
      let req_network = Option.is_some network in
      begin match req with
        | Rq_sign sir -> Lwt.return (Some sir, None, req_account, req_network)
        | Rq_send ser -> Lwt.return (None, Some ser, req_account, req_network)
        | _ ->
          let|> _ = Common.send_req ?id {r with Etypes.req} in
          (None, None, false, false)
      end
    | Some _ -> Lwt.return (None, None, false, false)
    | _ -> Lwt.return (None, None, false, false) in
  { l_account=account; l_network=network; l_accounts=accounts;
    l_node; l_sign_request; l_send_request; l_request_id = id;
    l_request_account; l_request_network }

let emit_logged app l =
  Vue_js.emit1 app "logged" (optdef logged_to_jsoo l)

let set_state app =
  app##.state := state_to_jsoo Blank;
  app##.password := string "";
  let> l_accounts = Storage.get_accounts () in
  match l_accounts with
  | [] ->
    app##.state := state_to_jsoo Enable;
    emit_logged app None;
    Lwt.return_unit
  | accounts ->
    let> password = get_password () in
    match password with
    | None ->
      app##.state := state_to_jsoo Unlock;
      emit_logged app None;
      Lwt.return_unit
    | Some _password ->
      let id = Optdef.to_option app##.id in
      let req = to_optdef request_of_jsoo app##.req in
      let> meta = match req with
        | Some {Etypes.req= Rq_permission {src = `api ({Jext_lwt.Types.url; _} as meta); _}; _} ->
          let> l = Storage.get_approved () in
          if List.mem url l then Lwt.return_none else Lwt.return_some meta
        | _ -> Lwt.return_none in
      match meta with
      | Some meta ->
        app##.state := state_to_jsoo @@ Approve meta;
        emit_logged app None;
        Lwt.return_unit
      | None ->
        let name, network = match req with
          | None -> None, None
          | Some req -> req.account, req.network in
        let> r = Storage.get_info ?name ?network accounts in
        match r with
        | Error ((`generic (name, message)) as e) ->
          app##.state := state_to_jsoo (State_error {name; message});
          begin match id with
            | None -> Lwt.return_unit
            | Some id ->
              let|> _ = Common.send_req ~id
                  {Etypes.req=Rq_callback_error e; account=None; network=None} in
              ()
          end
        | Ok info ->
          let|> logged = get_logged ?id ?req ~accounts ~account:info.i_account ~network:info.i_network false in
          emit_logged app (Some logged);
          app##.state := state_to_jsoo Info

let unlock app =
  app##.processing := _true;
  EzLwtSys.run @@ fun () ->
  let> () = EzLwtSys.sleep 0.02 in
  let password = to_string app##.password in
  let> accounts = Storage.get_accounts () in
  let> r = Common.get_sk ~password (List.hd accounts) in
  match r with
  | Error (`generic (name, message)) ->
    app##.state := state_to_jsoo (State_error {name; message});
    app##.processing := _false;
    Lwt.return_unit
  | Ok _ ->
    let> () = Ext.set_password (Some password) in
    app##.processing := _false;
    set_state app

let approve app =
  EzLwtSys.run @@ fun () ->
  match state_of_jsoo app##.state with
  | Approve meta ->
    let> () = Storage.set_approved meta in
    set_state app
  | _ -> set_state app

let reset app =
  EzLwtSys.run @@ fun () ->
  app##.password := string "";
  set_state app

let lock app =
  EzLwtSys.run @@ fun () ->
  let> () = Ext.set_password None in
  set_state app

let path app p _ =
  match to_string p with
  | "reset" -> reset app
  | "lock" -> lock app
  | _ -> ()

let set_network ?accounts ~account app v =
  let network = match to_optdef to_string v with
    | Some v -> v
    | None ->
      match to_string app##.network_select_ with
      | "custom" -> to_string app##.network_input_
      | s -> s in
  let> network = Storage.set_network network in
  let network = Result.value ~default:{n_url= EzAPI.BASE "http://tz.functori.com"; n_name=Some "mainnet"} network in
  let|> logged = get_logged ?accounts ~account ~network true in
  emit_logged app (Some logged)

let onclose id =
  Dom_html.window##.onunload := Dom_html.handler @@ fun _ ->
    EzLwtSys.run (fun () ->
        let|> _ = Common.send_req ~id {
            req=Rq_callback_error (`generic ("closed_notif", "notification was aborted"));
            account = None; network = None } in
        ());
    _true

module C = Vue_component.Make(struct
    type nonrec data = data
    class type all = object inherit data method locked: bool t prop end
    let name = "v-state"
    let element = Vue_component.CRender (Render.state_render, Render.state_static_renders)
    let props = Some (Vue_component.PrsArray ["path"])
  end)

let init () =
  C.add_method0 "approve" approve;
  C.add_method0 "unlock" unlock;
  C.add_method0 "set_state" (Common.runa0 set_state);
  C.add_watch "path" path;
  let file, id, req = get_req_info () in
  begin match id, req with
    | Some id, Some _ -> onclose id
    | _ -> ()
  end;
  let data _ = object%js
    val mutable password = string ""
    val mutable state = state_to_jsoo Blank
    val file = string file
    val id = Optdef.option id
    val req = optdef request_to_jsoo req
    val mutable processing = _false
  end in
  C.make ~data ~lifecycle:["created", Common.runa0 set_state] ()
