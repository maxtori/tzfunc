open Tzfunc
open Ezjs_min
open Rp
open Ext
open Etypes
open State

class type data = object
  method copy_message_ : js_string t prop
  method network_select_ : js_string t prop
  method network_input_ : js_string t prop
end

class type props = object
  method info : logged_jsoo t readonly_prop
  method file : js_string t readonly_prop
end

class type all = object
  inherit data
  inherit props
end

let expand _app =
  let|> _ = Chrome_lwt.Tabs.(create @@ make_create ~url:"index.html" ()) in
  Dom_html.window##close

let set_network app = Settings.set_network app

let route app s =
  Common.emit_routed app s

let set_account app a =
  let network = network_of_jsoo app##.info##.network in
  let account = account_of_jsoo a in
  let> () = Storage.set_selected account in
  let accounts = to_listf account_of_jsoo app##.info##.accounts in
  let|> logged = get_logged ~accounts ~account ~network true in
  State.emit_logged app (Some logged)

let xtz app =
  match to_optdef Proto_jsoo.account_of_jsoo app##.info##.node with
  | None -> def @@ string "--"
  | Some account ->
    let cs = Unsafe.global##._Intl##._NumberFormat in
    let intl = new%js cs (string "en-US") in
    let a = (Int64.to_float account.Proto.ac_balance) /. 1000000. in
    def @@ string @@ to_string (intl##format a) ^ " ꜩ"

module C = Vue_component.Make(struct
    type nonrec data = data
    type nonrec all = all
    let name = "v-header"
    let element = Vue_component.CRender (Render.header_render, Render.header_static_renders)
    let props = Some (Vue_component.PrsArray ["info"; "file"])
  end)

let init () =
  C.add_computed "xtz" xtz;
  C.add_method0 "expand" expand;
  C.add_method1 "copy" Common.copy;
  C.add_method1 "set_network" set_network;
  C.add_method1 "set_account" (Common.runa1 set_account);
  C.add_method1 "route" route;
  let data = object%js
    val mutable network_select_ = string ""
    val mutable network_input_ = string ""
    val mutable copy_message_ = string "copy"
  end in
  C.make ~data:(fun _ -> data) ()
