open Ezjs_min
open Tzfunc
open Rp

type micheline_input = {
  mi_input: Mtyped.ftype;
  mi_output: (Proto.micheline [@conv (Proto_jsoo.micheline_to_jsoo_aux ~remove_args:false, Proto_jsoo.micheline_of_jsoo)]);
  mi_kind: [ `tzip4 | `view of (Proto.micheline * Proto.micheline)];
} [@@deriving jsoo {modules=[Proto, Proto_jsoo; Mtyped, Mtyped_jsoo]}]

type data = {
  contract: string;
  views: (string * micheline_input) list; [@mutable] [@assoc]
  input: string;
  selected: string;
  param: string;
  amount: string;
  balance: string;
  source: string;
  payer: string;
  level: string;
  now: string;
  visible: bool; [@mutable]
  processing: bool; [@mutable]
  result: Proto.micheline option; [@mutable]
  error: Rp.error option; [@mutable]
} [@@deriving jsoo {modules=[Proto, Proto_jsoo; Rp, Rp_jsoo]}]

let empty = {
  contract = ""; views = []; input = ""; selected = ""; param = ""; amount = "";
  balance = ""; source = ""; payer = ""; level = ""; now = "";
  visible = false; processing = false; result = None; error = None }

let load_views app =
  let contract = to_string app##.contract in
  let>? l = Tzfunc.Node.get_entrypoints contract in
  let tzip4 = List.filter_map (fun (k, v) ->
      match Mtyped.parse_type v with
      | Error _ -> None
      | Ok mi_input ->
        match mi_input.Mtyped.typ with
        | `tuple [ mi_input; {Mtyped.typ=`contract _; _} ] ->
          let s = Mtyped.short mi_input in
          begin
            try Some (k, {mi_input; mi_output = Micheline.empty_micheline_value s; mi_kind=`tzip4})
            with _ -> None
          end
        | _ -> None) l in
  let|>? a = Tzfunc.Node.get_account_info contract in
  let views = match a.Proto.ac_script with
    | Some {Proto.code=Proto.Mseq l; _} ->
      List.filter_map (function
          | Proto.Mprim {prim=`view; args=[ Proto.Mstring k; input_type; output_type; _]; _} ->
            begin match Mtyped.parse_type input_type with
              | Error _ -> None
              | Ok mi_input ->
                let s = Mtyped.short mi_input in
                try Some (k, {mi_input; mi_output = Micheline.empty_micheline_value s; mi_kind=`view (input_type, output_type)})
                with _ -> None
            end
          | _ -> None) l
    | _ -> [] in
  app##.views := Table.makef micheline_input_to_jsoo (tzip4 @ views)

let view app =
  app##.processing := _true;
  app##.error := undefined;
  app##.result := undefined;
  EzLwtSys.run @@ fun () ->
  let> () = EzLwtSys.sleep 0.02 in
  let data = data_of_jsoo app in
  let err e =
    app##.error := def (Rp_jsoo.error_to_jsoo e);
    app##.processing := _false;
    Lwt.return_unit in
  if data.contract = "" then err @@ `generic ("empty contract", "")
  else
    let re = function
      | Error e -> err e
      | Ok m ->
        app##.result := def (Proto_jsoo.micheline_to_jsoo m);
        app##.processing := _false;
        Lwt.return_unit in
    let source = match data.source with "" -> None | s -> Some s in
    let payer = match data.payer with "" -> None | s -> Some s in
    let level = Int32.of_string_opt data.level in
    let now = try Some (Proto.A.cal_of_str data.now) with _ -> None in
    match data.input, List.assoc_opt data.selected data.views with
    | "", None ->
      app##.error := def (Rp_jsoo.error_to_jsoo (`generic ("no entrypoint given", "")));
      app##.processing := _false;
      Lwt.return_unit
    | e, None ->
      begin try
          let input = EzEncoding.destruct Proto.micheline_enc.Proto.json data.param in
          let> r = Utils.tzip4_view ~contract:data.contract ~input ?source ?payer ?level ?now e in
          re r
        with exn -> err @@ `generic ("unvalid parameter", Printexc.to_string exn)
      end
    | _, Some mi ->
      match mi.mi_kind with
      | `tzip4 ->
        let> r = Utils.tzip4_view ?source ?payer ?level ?now
            ~contract:data.contract ~input:mi.mi_output data.selected in
        re r
      | `view (input_type, output_type) ->
        let amount = Int64.of_string_opt data.amount in
        let balance = Int64.of_string_opt data.balance in
        let> r = Utils.view ?balance ?amount ?source ?payer ?level ?now
            ~contract:data.contract ~input_type ~output_type ~input:mi.mi_output data.selected in
        re r

module C = Vue_component.Make(struct
    type nonrec data = data_jsoo
    type nonrec all = data_jsoo
    let name = "v-view"
    let element = Vue_component.CRender (Render.view_render, Render.view_static_renders)
    let props = None
  end)

let init () =
  C.add_method0 "load_views" load_views;
  C.add_method0 "view" view;
  let data _ = data_to_jsoo empty in
  C.make ~data ()
