open Ezjs_min
open Tzfunc

class type all = object
  method input : Mtyped_jsoo.ftype_jsoo t readonly_prop
  method output : Proto_jsoo.micheline_jsoo t prop
  method index : int optdef readonly_prop
  method parent : Proto_jsoo.micheline_jsoo t js_array t optdef prop
end

let rec empty_micheline_value : Mtyped.stype -> Proto.micheline = function
  | `address | `chain_id | `key | `key_hash | `signature | `string | `timestamp
  | `l2_address -> Proto.Mstring ""
  | `bool -> Proto.prim `False
  | `bytes | `chest | `chest_key | `bls12_381_g1 | `bls12_381_g2 -> Proto.Mbytes (H.mk "")
  | `int | `mutez | `nat | `bls12_381_fr -> Proto.Mint Z.zero
  | `map (_k, _v) -> Proto.Mseq []
  | `option _t -> Proto.prim `None
  | `or_ (l, _) -> Proto.prim `Left ~args:[empty_micheline_value l]
  | `seq _t -> Proto.Mseq []
  | `tuple l -> Proto.Mseq (List.map empty_micheline_value l)
  | `unit -> Proto.prim `Unit
  | `big_map _ | `contract _ | `lambda _ | `never | `operation  | `sapling_state _
  | `sapling_transaction _ | `ticket _ ->
    log_str "cannot produce empty michline for this type";
    assert false

let append_micheline _app (typ : Mtyped_jsoo.ftype_jsoo t) (a : Proto_jsoo.micheline_jsoo t js_array t) =
  match Mtyped.short @@ Mtyped_jsoo.ftype_of_jsoo typ with
  | `seq t ->
    let v = Proto_jsoo.micheline_to_jsoo_aux ~remove_args:false @@ empty_micheline_value t in
    ignore (a##splice_1 (a##.length) 0 v)
  | `map (k, v) ->
    let k = empty_micheline_value k in
    let v = empty_micheline_value v in
    let x = Proto_jsoo.micheline_to_jsoo_aux ~remove_args:false @@ Proto.Mprim {prim=`Elt; args=[k; v]; annots=[]} in
    ignore (a##splice_1 (a##.length) 0 x)
  | _ -> log_str "cannot append for this input type"

let remove_micheline app =
  match Optdef.to_option app##.parent, Optdef.to_option app##.index with
  | Some parent, Some index ->
    let _ = parent##splice index 1 in ()
  | _ -> ()

let fill_options _app (t : Mtyped_jsoo.ftype_jsoo t) (v : Proto_jsoo.micheline_jsoo t) s =
  let prim = match to_string s, Mtyped_jsoo.ftype_of_jsoo t with
    | "none", {Mtyped.typ=`option _; _} ->
      let _ = (Unsafe.coerce v)##.args##splice 0 1 in
      string "None"
    | "some", {Mtyped.typ=`option t; _} ->
      let x = Proto_jsoo.micheline_to_jsoo_aux ~remove_args:false @@ empty_micheline_value @@ Mtyped.short t in
      let _ = (Unsafe.coerce v)##.args##splice_1 0 0 x in
      string "Some"
    | "left", {Mtyped.typ=`or_ (l, _); _} ->
      let x = Proto_jsoo.micheline_to_jsoo_aux ~remove_args:false @@ empty_micheline_value @@ Mtyped.short l in
      let _ = (Unsafe.coerce v)##.args##splice_1 0 1 x in
      string "Left"
    | "right", {Mtyped.typ=`or_ (_, r); _} ->
      let x = Proto_jsoo.micheline_to_jsoo_aux ~remove_args:false @@ empty_micheline_value @@ Mtyped.short r in
      let _ = (Unsafe.coerce v)##.args##splice_1 0 1 x in
      string "Left"
    | _ -> string "unknown" in
  (Unsafe.coerce v)##.prim := prim

let hex_or_string _app s =
  let s2 = to_string s in
  let t = String.fold_left (fun acc c ->
      acc && (let i = Char.code c in i >= 48 && i <= 57 || i >= 97 && i <= 102)) true s2 in
  if t then s
  else
    let `Hex h = Hex.of_string s2 in string h

module C = Vue_component.Make(struct
    class type data = object end
    type nonrec all = all
    let name = "v-micheline"
    let element = Vue_component.CRender (Render.micheline_render, Render.micheline_static_renders)
    let props = Some (Vue_component.PrsArray ["input"; "output"; "index"; "parent"])
  end)

let init () =
  C.add_method2 "append" append_micheline;
  C.add_method0 "remove" remove_micheline;
  C.add_method2 "fill" fill_options;
  C.add_method1 "hex" hex_or_string;
  C.make ()
