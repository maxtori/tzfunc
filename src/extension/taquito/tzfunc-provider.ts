import { WalletProvider, WalletDelegateParams, WalletOriginateParams, WalletTransferParams, Signer } from '@taquito/taquito'
import { b58cdecode, b58cencode } from '@taquito/utils'
import { MichelsonV1Expression } from '@taquito/rpc'

export interface SignRequest {
  bytes: string;
  watermark?: string;
  type?: MichelsonV1Expression;
}

export interface Parameters {
  entrypoint: string;
  value: MichelsonV1Expression
}

export interface TransactionInfo {
  amount: string;
  destination: string;
  parameters?: Parameters;
}

export interface Script {
  code: MichelsonV1Expression;
  storage: MichelsonV1Expression;
}

export interface OriginationInfo {
  balance: string;
  script: Script;
}

export interface Transaction { transaction: TransactionInfo }
export interface Origination { origination: OriginationInfo }
export interface Delegation { delegation: string | null }
export interface Reveal { reveal: string }

export type OperationKind = Transaction | Origination | Delegation | Reveal

export type Operation = OperationKind & {
  source?: string;
  fee?: string;
  gas_limit?: string;
  storage_limit?: string;
  counter?: string;
}

export interface SendRequest {
  operations: Operation[];
  forge_method?: 'remote' | 'local' | 'both' ;
  remove_failed?: boolean;
}

export interface Rpc { rpc: string }
export interface Sign { sign: SignRequest }
export interface Forge { forge: SendRequest }
export interface Inject { inject: string }
export interface Send { send: SendRequest }
export interface Info { info: number }

export type RequestKind = Rpc | Sign | Forge | Inject | Send | Info

export interface Request {
  req: RequestKind;
  network?: string;
  account?: string;
}

export interface RpcResult { rpc: any }
export interface SignResult { sign: string }
export interface InjectResult { inject: string }
export interface OperationResult {
  branch: string;
  protocol: string;
  bytes: string;
  operations: Operation[];
}
export interface ForgeResult { forge: OperationResult }
export interface SendResult { send: OperationResult & { hash: string } }
export interface Network {
  url: string;
  name?: string;
}
export interface InfoAuxResult {
  pkh: string;
  pk: string;
  network: Network;
}
export interface InfoResult { info: InfoAuxResult }
export type TResult = RpcResult | SignResult | InjectResult | ForgeResult | SendResult | InfoResult

export interface TzFunc {
  request: (request: Request) => Promise<TResult>
}

const factor = 1000000

export class TzFuncWallet implements WalletProvider {
  private api: TzFunc;

  constructor(api: TzFunc) {
    this.api = api
  }

  async getPKH() : Promise<string> {
    const res = await this.api.request( { req: { info: 0 } }) as InfoResult
    return res.info.pkh
  }

  async mapDelegateParamsToWalletParams(params: () => Promise<WalletDelegateParams>) : Promise<Operation> {
    const p = await params()
    const delegation = (p.delegate) ? p.delegate : null
    const gas_limit = (p.gasLimit) ? p.gasLimit.toString() : undefined
    const storage_limit = (p.storageLimit) ? p.storageLimit.toString() : undefined
    const fee = (p.fee) ? (p.fee * factor).toString() : undefined
    return { delegation, gas_limit, storage_limit, fee }
  }

  async mapOriginateParamsToWalletParams(params: () => Promise<WalletOriginateParams>) : Promise<Operation> {
    const p = await params()
    const gas_limit = (p.gasLimit) ? p.gasLimit.toString() : undefined
    const storage_limit = (p.storageLimit) ? p.storageLimit.toString() : undefined
    const fee = (p.fee) ? (p.fee * factor).toString() : undefined
    const script : Script = { code: p.code as MichelsonV1Expression, storage: p.init as MichelsonV1Expression }
    return { origination: {balance: p.balance || "0", script}, gas_limit, storage_limit, fee }
  }

  async mapTransferParamsToWalletParams(params: () => Promise<WalletTransferParams>) : Promise<Operation> {
    const p = await params()
    const amount = (p.amount * factor).toString()
    const destination = p.to
    const parameters = p.parameter
    const gas_limit = (p.gasLimit) ? p.gasLimit.toString() : undefined
    const storage_limit = (p.storageLimit) ? p.storageLimit.toString() : undefined
    const fee = (p.fee) ? (p.fee * factor).toString() : undefined
    return { transaction: {amount, destination, parameters}, gas_limit, storage_limit, fee }
  }

  async sendOperations(operations: Operation[]) : Promise<string> {
    const r = await this.api.request({req: { send : { operations } } }) as SendResult
    return r.send.hash
  }

}

function uint8array_to_hex(a: Uint8Array) : string {
  return a.reduce((acc, x) => acc + x.toString(16).padStart(2, '0'), '')
}

export class TzFuncSigner implements Signer {
  private api: TzFunc;

  constructor(api: TzFunc) {
    this.api = api
  }

  async publicKey() : Promise<string> {
    const res = await this.api.request( { req: { info: 0 } }) as InfoResult
    return res.info.pk
  }

  async publicKeyHash() : Promise<string> {
    const res = await this.api.request( { req: { info: 0 } }) as InfoResult
    return res.info.pkh
  }

  async secretKey() : Promise<string | undefined> {
    return undefined
  }

  async sign(bytes: string, magicByte?: Uint8Array) : Promise<{ bytes: string; prefixSig: string; sbytes: string; sig: string }> {
    const watermark = (magicByte) ? uint8array_to_hex(magicByte) : undefined
    const r = await this.api.request( { req: { sign: {bytes, watermark} } }) as SignResult
    const prefixSig = r.sign
    const sig_bytes = b58cdecode(prefixSig, new Uint8Array([9, 245, 205, 134, 18]))
    const sbytes = bytes + uint8array_to_hex(sig_bytes)
    const sig = b58cencode(sig_bytes, new Uint8Array([4, 130, 43]))
    return {bytes, prefixSig, sbytes, sig}
  }

}
