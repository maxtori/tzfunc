const browser_api = (chrome==undefined) ? browser : chrome

function inject_api(file="js/api.js", msg) {
  if (msg != undefined) console.log(msg);
  let script = document.createElement("script")
  script.src = browser_api.runtime.getURL(file);
  (document.head||document.documentElement).appendChild(script);
}

function site_name() {
  let meta = document.querySelector('head > meta[property="og:site_name"]');
  if (meta != undefined) return meta.content
  else {
    let meta = document.querySelector('head > meta[name="title"]');
    if (meta != undefined) return meta.content
    else return document.title
  }
}

function site_icon() {
  let icon = document.querySelector('head > link[rel="shortcut icon"]');
  if (icon != undefined) return icon.href;
  else {
    let icon = document.querySelector('head > link[rel="icon"]');
    if (icon != undefined) return icon.href;
    else return undefined;
  }
}

function site_metadata() {
  return {
    name: site_name(),
    icon: site_icon(),
    url: location.hostname
  }
}

function main(file, msg, api_src="api") {
  let info = { name: api_src };
  let port = browser_api.runtime.connect(undefined, info);
  port.onMessage.addListener(function(msg) { window.postMessage(msg) });
  window.addEventListener("message", function(ev) {
    if (ev.srcElement == window && ev.data.src == api_src) {
      let req = ev.data;
      req.src = {api: site_metadata()};
      port.postMessage(req);
    }
  });
  inject_api(file, msg)
}

main();
