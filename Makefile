all: build

build:
	@dune build --profile release

clean:
	@dune clean
	@rm -rf public
	@rm -rf src/extension/taquito/dist src/extension/taquito/node_modules src/extension/taquito/package-lock.json

build-deps:
	@eval $(opam env)
	@opam install . --deps-only

install:
	@opam install .

doc:
	@dune build --profile release @doc
	@cp -rf _build/default/_doc/_html public
	@cp -f docs/index.html public/tzfunc

taquito:
	@npm run build --prefix src/extension/taquito
